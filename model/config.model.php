<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2018-03-08 11:39:04
#path : app/model/config.class.php
*/
class configModel extends db
{
	function getTypeList()
	{
		$rs = $this->order("type,porder,id")
				->as('a')		
				->field("a.type")
				->findAll();
		$rec = array();
		foreach($rs as $k => $v)
		{
			$rec[$v['type']] = $v['type'];
		}
		return $rec;
	}
	function getMaxOrder()
	{
		$rt = $this
			->field("MAX(`porder`) as max")
			->find();
		return $rt['max'];
	}
	/**
	参数: 模型返回此TAG的记录
	*/
	function check($tag)
	{
		$rt = $this->where("`key`='{$tag}'")->find();
		
		return $rt;
	}

}