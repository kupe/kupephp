<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2017-11-02 10:55:00
#path : app/model/group.class.php
*/
class groupModel extends db
{
	function getGroup()
	{
		$rec = array();
		$rs = $this->order("porder,id")->findAll();
		foreach($rs as $k => $v)
		{
			$rec[$v['tag']] = $v['name'];
		}
		return $rec;
	}
	function getGroupId()
	{
		$rec = array();
		$rs = $this->order("porder,id")->findAll();
		foreach($rs as $k => $v)
		{
			$rec[$v['id']] = $v['name'];
		}
		return $rec;
	}
}