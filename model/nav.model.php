<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2018-03-11 11:53:47
#path : app/model/nav.model.php
*/
class navModel extends db
{
	function getType()
	{
		$db = m('mode');
		$rs = $db->getMode();
		$rec = array();
		//tag |  | id |
		foreach($rs as $k => $v)
		{
			$rec[$v['tag'] . "|category|{$v['id']}"] = $v['name'] . "分类";
		}
		foreach($rs as $k => $v)
		{
			$rec[$v['tag'] . "|content|{$v['id']}"] = $v['name'] . "页面";
		}
		$rec["my"] = "自定义网址";

		return $rec;
	}
	/**
	$modes m('mode')->getMode();
	*/
	function getTypeKey($rt, $modes = false)
	{
		if(!$modes)
		{
			$modes = m('mode')->getMode();
		}
		if($rt['type'] == 'my')
		{
			$key = 'my';
		}
		elseif($rt['type'] == 'content')
		{
			$key = $modes[$rt['mid']]['tag'] . "|content|{$rt['mid']}";
		}
		elseif($rt['type'] == 'category')
		{
			$key = $modes[$rt['mid']]['tag'] . "|category|{$rt['mid']}";
		}
		else
		{
			$key = '';
		}
		return $key;

	}

	function dealNavUrl($rs)
	{
		$num = 1;
		$db2 = m('mode');
		$modes = $db2->getMode();
		foreach($rs as $k => $v)
		{
			$v['num'] = $num++;
			if($v['type'] == 'category')
			{
				$v['type2'] = "分类:{$v['category_name']}";
				$v['url'] = "{$v['name2']}.html";
			}
			elseif($v['type'] == 'content')
			{
				$table = "my_{$modes[$v['mid']]['tag']}";
				$rt = $db2->table($table)->pk($v['news_id'])->find();
				$v['type2'] = "内容:{$rt['title']}";

				if($rt['name2'])
				{
					$v['url'] = "{$rt['name2']}.html";
				}
				else
				{
					isset($modes[$v['mid']]) && $v['url'] = $modes[$v['mid']]['tag'] ."-{$v['news_id']}.html";
				}
				
			}
			else
			{
				$v['type2'] = "自定义";				
			}

			$rs[$k] = $v;
		}
		return $rs;
	}
}