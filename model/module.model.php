<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2017-10-27 12:51:33
#path : app/model/module.class.php
*/
class moduleModel extends db
{
	public $pk = 'module';
	function getMaxOrder()
	{
		$rt = $this
			->field("MAX(`porder`) as max")
			->find();
		return $rt['max'];
	}
	function getMaxOrderOp()
	{
		$rt = $this
			->table("module_op")
			->field("MAX(`porder`) as max")
			->find();
		return $rt['max'];
	}
	/** 验证操作是否存在 */
	function checkOpExists($input)
	{
		$rt = $this->table('module_op')
			->where("`module_id`='{$input['module_id']}' AND `method`='{$input['method']}'")
			->find();
		return $rt;
	}
	function checkExists($input)
	{
		$rt = $this->where("module='{$input['module']}'")
			->find();
		return $rt;
	}
	/**
	获取所有操作*/
	function getOpAll()
	{
		$rs = array();
		$tmp = $this->table('module_op')
			->order('is_menu desc,porder,is_menu desc, status desc, name asc')
			->findAll();

		foreach($tmp as $k => $v)
		{
			if($v['tpl'])
			{
				$v['tpl'] = htmlspecialchars_decode($v['tpl']);
			}
			$rs[$v['module_id']][] = $v;
		}
		return $rs;
	}
	/**删除模型 和 模型下的字段*/
	function del($module)
	{
		$this->where("module='{$module}'")->delete();
		$this->table('module_op')
			->where("module='{$module}'")
			->delete();
		return true;
	}
	function delop($opid)
	{
		$this->table('module_op')
			->where("id='{$opid}'")
			->delete();
		return true;
	}

	#####################
	/**
	获取管理后台的所有模块
	*/
	function getAdminOp()
	{
		return $this->getOp("is_admin=1 AND");
	}
	/**
	获取管理后台的菜单, 只返回是菜单的模块
	#需要根据用户权限
	*/
	function getAdminMenu()
	{
		if($_SESSION['user']['tag'] == 'all')
		{
			$key = "is_admin=1 AND is_menu=1 AND";	
			if(!$rs = load::loadClass('cacheing')->get($key))
			{
				$rs = $this->getOp($key);
				$this->cacheing->add($key,$rs);
			}	
		}
		else
		{
			/**$rs = $this->where(" status=1")
				->as('a')
				->table("module_op")
				->leftJoin("module as b", "a.module_id=b.module")
				->where("b.tag like '%{$_SESSION['user']['tag']}%'")
				->getOpAll();*/
				$key = "is_admin=1 AND is_menu=1 AND". " AND tag like '%{$_SESSION['user']['tag']}%'";

				if(!$rs = $this->cacheing->get($key))
				{
					$rs = $this->getOp("is_admin=1 AND is_menu=1 AND", " AND tag like '%{$_SESSION['user']['tag']}%'");			
					$this->cacheing->add($key,$rs);
				}
		}
		
		if($_SESSION['user']['auth'] == '*')
		{
			return $rs;
		}
		$rec = array();	
		
		foreach($rs as $k => $v)
		{
			foreach($v['op'] as $kk => $vv)
			{
				$key = "{$k}_{$vv['method']}";
				
				if(!in_array($key, $_SESSION['user']['auth']))
				{
					
					unset($rs[$k]['op'][$kk]);
				}
			}
		}
	//	p($rs);
		return $rs;

		
	}
	/*
	$where 操作METHOD 的查询条件
	$module_where 大的一级模块 查询的条件
	*/
	function getOp($where = '', $module_where = null)
	{
		$rs = $this->where("status=1 {$module_where}")
			->order("porder,add_time")
			->findAll();

		$tmp = $this->where("{$where} status=1")->getOpAll();
		$ops = array();
		foreach($tmp as $k => $v)
		{
			foreach($v as $kk => $vv)
			{
				$ops[$k][$vv['method']] = $vv;
			}
		}

		//p($rs);

		$rec = array();

		foreach($rs as $k => $v)
		{
			if(isset($ops[$v['module']]))
			{
				$v['op'] = $ops[$v['module']];
				$rec[$v['module']] = $v;
			}
			else
			{
				$v['op'] = array();
				$rec[$v['module']] = $v;
			}
		}

		return $rec;
	}
	/**
	处理模块的标签, 根据一级模块中写的标签,算出
	*/
	public function getTag()
	{
		$rs = $this->where("status=1")
			->order("porder")
			->field("tag")
			->findAll();
		$tag = "";
		foreach($rs as $k => $v)
		{
			$tag .= trim($v['tag']) . " ";
		}
		$rec = array();
		foreach(explode(' ', $tag) as $k => $v)
		{
			if(!$v = trim($v))
			{
				continue;
			}
			$rec[$v] = $v;
		}
		return $rec;
	}
}