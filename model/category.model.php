<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2018-03-05 12:46:25
#path : app/model/category.class.php
*/
class categoryModel extends db
{
	#每個分类的文章数统计
	public $total = array();


	/**刪除 分类的逻辑处理*/
	function dodelcategory($id)
	{
		$tree = $this->getTreeObj();
		$ids = $tree->getSubId($id);
		if(count($ids))
		{
			$this->where("id IN (".implode(',',$ids).")")->delete();
			//删除文章
			//$this->table('article')->where("category_id IN (".implode(',',$ids).")")->delete();
		}
	}	
	/**计算各分类的文章数*/
	function getTotal($table)
	{
		$tmp = $this->table($table)->field("count(*) as total,category_id")
			->group('category_id')->findAll();
		$total = array();
		foreach($tmp as $k => $v)
		{
			$total[$v['category_id']] = $v['total'];
		}
		$this->total = $total;
		return $total;
	}
	/**
	返回 树型  类
	*/	
	function getTreeObj()
	{
		$tree_data = $this->getData();
		$tree = load::loadClass('tree',null, array($tree_data));
		return $tree;
	}


	public $subids;
	function getMenuByTag($tag)
	{
		return $this->table("menu")->where("tag='{$tag}' AND fid=-1")->find();
	}

	//返回下拉菜单
	//show=1
	function getSelectByTag($tag,$v = array(
	'name' => 'category_id',
	'value' => '',
	))
	{
		extract($v);
		if(isset($show))
		{
			$wstr = " AND a.show=".intval($show);
		}
		else
		{
			$wstr = '';
		}
		#数据源
		$rs = $this->as('a')
			->InnerJoin("menu as b","a.menu_id=b.id")
			->field("a.*")
			->where("b.tag='{$tag}' AND b.fid=-1 {$wstr}")->order('a.fid,a.porder,a.id')->findall();
		#TEST
		import('ext.tree');
		$tree= new Tree($rs);
		$html = $tree->select($v);
		return $html;
	}
	#分类中的文章数
	function getCountOfCategory($menu_id)
	{
		$rs = $this->field("count(*) as total,category_id")
			->group("category_id")
			->table('article')
			->as('a')
			->leftjoin("category as b","a.category_id=b.id")
			->where("b.menu_id='{$menu_id}'")
			->findAll();
		$rec = array();
		foreach($rs as $k => $v)
		{
			$rec[$v['category_id']] = $v['total'];
		}
		return $rec;

	}
	/**
	参数: 
	*/
	function check($name2)
	{
		$rt = $this->where("`name2`='{$name2}'")->find();
		return $rt;
	}




	##################OLD
	function GetCategoryName($where = '')
	{
		$rec = array();
		$rs = $this->field('id,name')->where($where)->order('name')->findAll();
		//$rec = array('0' => '++根分类++');
		foreach ($rs as $k => $v)
		{
			$rec[$v['id']] = $v['name'];
		}
		return $rec;
	}
	function GetCategorySelect($where = '', $value = '', $name = 'category_id', $default = '所有...')
	{
		$rs = $this->GetList();

		$out = "<select name='{$name}'>";
		if (isset($default))
		{
			$out .= "<option value=''>{$default}</option>";
		}
		foreach ($rs[0] as $k => $v)
		{

			$out .= "<optgroup label='{$v['name']}'>";
			foreach ($rs[1][$v['id']] as $kk => $vv)
			{
				if ($value && $vv['id'] == $value)
				{
					$out .= "<option value='{$vv['id']}' selected  style='color:red'>|--{$vv['name']}</option>";
				}
				else
				{
					$out .= "<option value='{$vv['id']}' style='color:blue'>|--{$vv['name']}</option>";
				}
			}
			$out .= '</optgroup>';
		}
		$out .= "</select>";
		return  $out;
	}
	

	function getData()
	{
		static $rec = array();
		if (count($rec))
		{
			return $rec;
		}
		$rs = $this->order("porder, id")->findAll();
		$rec = array();
		foreach ($rs as $v)
		{
			if(count($this->total))
			{
				if(isset($this->total[$v['id']]))
				{
					$v['total'] = $this->total[$v['id']];
				}
				else
				{
					$v['total'] = 0;
				}
			}
			$rec[$v['id']] = $v;
		}
		return $rec;
	}
	#根据一个ID, 取得从下到上的分类  从下 => 上
	function getTopCats($id)
	{
		$data = $this->getData();
		$rec = array();
		while (1)
		{
			if (isset($data[$id]))
			{
				$rt = $data[$id];
			}
			else
			{
				break;
			}
			$rec[$id] = $rt;
			if ($rt['fid'] == 0)
			{
				break;
			}
			else
			{
				$id = $rt['fid'];
			}
		}
		$rec = array_reverse($rec);
		return $rec;
	}
	#别名
	function topcategory($id)
	{
		return $this->getTopCats($id);
	}
	/*参数:一个分类ID,  返回:其子分类下的所有ID*/
	function subcategory($fid = 0)
	{
		//static $cnt = 0;
		$this->__subcategory($fid, $this->getData());
		$ids = $this->subids;
		$this->subids = array();
		return $ids;
	}
	function __subcategory($fid = 0, $categorys)
	{
		//static $cnt = 0;
		$categorys = $this->getData();
		foreach ($categorys as $id => $v)
		{
			//unset($categorys[$id]);
			if ($v['fid'] == $fid)
			{
				if (!$v['is_last'])
				{
					$this->subids[] = $v['id'];
					$this->__subcategory($v['id'], $categorys);
				}
				else
				{
					$this->subids[] = $v['id'];
				}
				//$this->subcategory($v['id'], $categorys);
			}
		}
	}

}