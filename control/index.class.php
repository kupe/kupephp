<?php
/**
默认控制器
*/
/*
Warning
This feature has been DEPRECATED as of PHP 7.2.0. Relying on this feature is highly discouraged.
function __autoload($name,$b)
{
	echo $name;
}
*/
require('library/front.class.php');

class index extends front
{
	function indexAction()
	{
		
		$this->setSeo();
		$this->display("index.html");
	}
	/**
	功能说明:	
	创建日期:2018-03-15
	*/
	public function categoryAction()
	{
		$id = intval($this->security->get('id')); 
		$global = $this->frontapi->global;

		if(!isset($global['category'][$id]))
		{
			$this->error("分类ID:{$id}不存在");
		}
		$category = $global['category'][$id];
		$category['title'] = $category['name'];
		if(!$category['show'])
		{
			$this->error("分类ID:{$id} 不可用");
		}
		if(!isset($global['mode'][$category['mid']]))
		{
			$this->error("模型ID:{$category['mid']} 不可用");
		}
		$mode = $global['mode'][$category['mid']];
		$table = "my_{$mode['tag']}";

		if($mode['tpl_category'])
		{
			$tpl = $mode['tpl_category'];
		}
		else
		{
			$tpl = "category.html";
		}

		$this->setSeo($category);
		$this->tpl->assign('CATEGORY', $category);
		$this->tpl->assign('RT', $category);
		$this->tpl->assign('MODE', $mode);

		###################此分类下的所有记录
		$tree = load::loadClass('tree',null, array($global['category']));

		//当前页数
		$page = intval($this->security->get('page'));
		//每页显示 $limit 条
		$limit = intval($this->security->get('limit'));
		//权限的意思: 自己不能编辑自己, 可以管理等级比自己低的用户
		$wstr = "1 ";
			

		if($id)
		{
			$subids = $tree->getSubId($id);
			$subids[] = $id;
			$wstr .= " AND a.category_id IN (".implode(',',$subids).")";
		}
		$db = m('config');

		$total = $db->table($table)->as('a')->where($wstr)->count();
		//$r = $this->getLimit($page,$limit, $total);

		$r = $this->page($total,"&id=$id",2);

		$rs = $db->order("id desc,title")
			->limit($r['limit'])
			->as('a')
			->table($table)
			->where($wstr)		
			//->leftJoin('category as b','a.category_id=b.id')
			//->field("a.*,b.name as category_name,b.id as category_id")				
			->findAll();

		$num = 1;
		foreach($rs as $k => $v)
		{
			$v['num'] = $num++;
			$v['category_name'] = $global['category'][$v['category_id']]['name'];
			if($v['name2'])
			{
				$v['url'] = $v['name2'] . ".html";
			}
			else
			{
				$v['url'] = "{$mode['tag']}-{$v['id']}.html";
			}
			$rs[$k] = $v;
		}
		
		$this->tpl->assign('page',$r['page']);
		$this->tpl->assign('RS',$rs);
		$this->tpl->assign('total',$r['total']);

		

		$this->display($tpl);
	}
	function error($msg)
	{
		echo $msg;
		exit;
	}
	
	/**
	功能说明:	显示具体内容
	创建日期:2018-03-15
	*/
	public function contentAction()
	{
		$id = intval($this->security->get('id')); 
		$modetag = $this->security->get('mode'); 

		$global = $this->frontapi->global;
		$table = "my_{$modetag}";
		if(!isset($global['modetag'][$modetag]))
		{
			$this->error("模型:{$modetag}不存在");			
		}
		$mode = $global['modetag'][$modetag];

		if($mode['tpl_content'])
		{
			$tpl = $mode['tpl_content'];
		}
		else
		{
			$tpl = "content.html";
		}
		$db = m('config');
		$rt = $db->table($table)->pk($id)->find();
		if(!$rt)
		{
			$this->error("记录不存在 404");
		}
		
		$this->setSeo($rt);

		$rt['content'] = html_entity_decode($rt['content']);

		

		$this->tpl->assign('MODE', $mode);
		$this->tpl->assign('RT', $rt);

		$this->display($tpl);
	}	
	function setSeo($rt = null)
	{
		if(!$rt)
		{
			$rt = [
			'title' => 	$this->frontapi->g('site_title'),
			'keyword' => 	$this->frontapi->g('site_keyword'),
			'desc' => 	$this->frontapi->g('site_desc'),
			];
		}
		$this->tpl->assign('title', $rt['title']);
		$this->tpl->assign('keyword', $rt['keyword']);
		$this->tpl->assign('desc', $rt['desc']);
	}
	function addAction()
	{
		$data['m'] = $this->input->get('m');
		$this->input->code('','','get');
		$this->output->p($data);
		$this->help->input();
		$this->help->out();

		//$this->tpl->display("template/default/add.html");
	}
}