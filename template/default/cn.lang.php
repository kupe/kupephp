id	= "ID"
login	= 用户名
name	= 管理员
group_id =	所在分组
status	= 状态
phone	= 电话
mobile	= 手机
qq	= QQ
address	= 地址
reg_ip	= 注册IP
reg_date = 注册时间




[index]
iplimit = " IP限制系统"
iplimit.desc = " IP限制系统由本工作室独立开发.采用PHP+MYSQL+MEMCACHE结合, 采用了大量的优化技巧,内存缓存技术,大大提高了程序的执行效率, 数月的开发与完善,目前已进入维护期. 网站程序拥有强大的管理后台,人性后的设计,方便管理员实时统计,监听自己网站目前的访问情况"


web = "PHP网站开发"
web.desc="承接各种企业,个人站长网站开发.展示型企业网站,定制各类网站,按用户需求建站"

php = "ERP开发"
php.desc="承接各类企业,单位的内部定制ERP系统,文件管理下载系统.擅长定制开发进销存系统,有需要可联系看案例"


tec = "技术支持"
tec.desc = "专业为你解决LINUX,PHP,APACHE,JAVASCRIPT,HTML,<br>MEMCACHE,网站SEO等相关领域的技术难题"


/*:<script src='http://api.kupebank.com/phone.php?a=phone'></script>*/

ad1 = "专注PHP开发与推广"
ad1.desc = "在知识爆炸的年代，我们不愿成为知识的过客，拥抱开源文化，发挥社区的力量，KupePHP框架代码开源,托管在。
		https://gitee.com/kupe/kupephp"

about = "新的一年,新的十年,启程!!!"
about.desc = "工作室成立于2008年,主创人员都为80后的有志之士.
开发优秀的PHP程序,推广PHP语言,是我们的目标.
本工作室所有程序,程序设计思想,均为原创, 先后自主开发了: 国内IP限制系统,123上网代理,链接交换系统,铭扬充值系统,各类ERP程序,和网站程序
我们希望通过自己的努力,开创一片天地. <!---特别得感谢不断给我们帮助的:娟娟,小猪,天行数码,TT,toneton,诚心.还有周工,杨工,大仙等通力支持.-->
新的开始,新的起点,我们依然走原创开发路线, 工作室本着客户至上,服务至上的原则,真诚为每一位客户服务.
2018新的一年,新的十年,启程!!!"