<?php
/**

配置
*/

return array(

/* 框架模式 
空 正常模式
develop => 开发模式
*/
'kupe_status' => 'develop',

/**数据库配置*/
'db_host' => '127.0.0.1',
'db_user' => 'root',
'db_password' => 'root',
'db_name' => 'kupe',
'db_prefix' => 'z_',

'db_type' => 'mysql',
'db_charset' => 'utf8',
'db_port' => '3306',

/** PHP文件存在目录,模板文件存放目录*/
'control_dir' => 'control',
'template_dir' => 'default',

#兼容
'cacheing' => array(
	'class' => 'memcache',
	'class' => 'file',
	'file' => array('path' => 'data/cache/'),
	'memcache' => array('host' => '127.0.0.1','port' => '11211'),

),



//数据模板所在目录
	"dir_model" => 'model',
	//视图所在目录
	"dir_view" => 'template',
	//当前使用的皮肤
	"dir_skin" => 'default',

/** URL配置*/
	
	'url' => array(
	
	#GET,获取的KEY
	"c" => 'c',
	"m" => 'm',

	#无值时,默认
	"default_c" => 'index',

	"default_m" => 'index',

	#最小操作 method的后缀
	
	"method_prefix" => 'Action',

	),




/**模板配置*/

'tpl' => array(
	#模板编译目录
	"compile_dir" => APPPATH."/data/compile",
	#模板编译文件的前缀

	//对php,html压缩,去掉html注释
	"zip_php" => false,
	"zip_html" => false,
	//每次判断一个模板
	"check" => true,
	//强制编译模板
	"force_compile" => false,

),


);