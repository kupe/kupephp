<?php
/***
测试,扩展
处理框架的用户输入,用 security 更好
*/
class EXT_input
{
	function get($key)
	{
		return $_GET[$key];
	}
	function post($key)
	{
		return $_POST[$key];
	}
	function is_get($key)
	{
		return isset($_GET[$key]);
	}
	function is_post($key)
	{
		return isset($_POST[$key]);
	}
	function cookie($key)
	{
		return $_COOKIES[$key];
	}
	/**
	只得到数据
	*/
	function vars($key)
	{		
		return $_POST[$key] ? $_POST[$key] : $_GET[$key];
	}
	function safeString($str)
	{
		$str = preg_replace('/[^a-zA-Z0-9]/isU','',$str);
		return $str;
	}
	function code($explain = 0,  $method = 'POST')
	{
		$mark = '<br>';
		if ($method == 'POST')
		{
			echo "\$input = array(); {$mark}\n";
			foreach ($_POST as $k => $v)
			{
				if ($explain)
				{
					echo "\$input['{$explain}_{$k}'] = \$this->security->post('$k'); {$mark}\n";
				}
				else
				{
					echo "\$input['{$k}'] = \$this->security->post('$k'); {$mark}\n";
				}
			}
		}
		else
		{
			echo "\$input = array(); {$mark}\n";
			foreach ($_GET as $k => $v)
			{
				if ($explain)
				{
					echo "\$input['{$explain}_{$k}'] = \$this->security->get('$k'); {$mark}\n";
				}
				else
				{
					echo "\$input['{$k}'] = \$this->security->get('$k'); {$mark}\n";
				}
			}
		}
	}
}