<?php
/*doc
$file = new files;
->dirFormat()	格式化目录变量：1、分隔符统一替换为'/'2、末尾统一去掉'/'

->getFile()
->getDir()
doc*/
/*

$http = ext('ext.http');


  [url] => http://www.a.com/info.php
            [content_type] => text/html
            [http_code] => 200
            [header_size] => 232
            [request_size] => 217
            [filetime] => -1
            [ssl_verify_result] => 0
            [redirect_count] => 0
            [total_time] => 0.012
            [namelookup_time] => 0
            [connect_time] => 0
            [pretransfer_time] => 0
            [size_upload] => 0
            [size_download] => 62478
            [speed_download] => 5206500
            [speed_upload] => 0
            [download_content_length] => 0
            [upload_content_length] => 0
            [starttransfer_time] => 0.007
            [redirect_time] => 0

*/
class EXT_files
{
	function getDir($path)
	{
		$path = $this->dirFormat($path);
		//$path = iconv("UTF-8","gbk//IGNORE",$path);
		$fp = opendir($path);
		$rec = array();
		while($file = readdir($fp))
		{
			if($file{0}=='.')
			{
				continue;
			}
			if(is_dir($path . '/' .$file))
			{
				$rec[] = $file;
			}
		}
		return $rec;
	}
	function getFile($path)
	{
		$path = $this->dirFormat($path);
		//$path = iconv("UTF-8","gbk//IGNORE",$path);
		$fp = opendir($path);
		$rec = array();
		while($file = readdir($fp))
		{
			if($file{0}=='.')
			{
				continue;
			}
			if(is_file($path . '/' .$file))
			{
				$rec[] = $file;
			}
		}
		return $rec;
	}
	
	//获取某个目录下的所有文件,并展示文件大小,修改时间等信息
	function getFileInfo($path)
	{
		$rs = $this->getFile($path);
		$rec = [];
		foreach($rs as $k => $v)
		{
			$rt['name'] = $v;
			$rt['mtime'] = date("Y-m-d H:i:s",filemtime($path . "{$v}"));
			$rt['size'] = round(filesize($path . "{$v}")/1024,1);
			$rt['ext'] = $this->getExt($v);

			$rec[] = $rt;
		}
		return $rec;
	}

	function getExt($file)
	{
		return @strtolower(end(explode('.',$file)));
	}

	/**
 * 格式化目录变量：1、分隔符统一替换为'/'2、末尾统一去掉'/'
 * @param string $dir 待格式化的字符串
 * @return string
 */
function dirFormat($dir){
    DIRECTORY_SEPARATOR!='/' and $dir=str_replace(DIRECTORY_SEPARATOR, '/', $dir);
    substr($dir,-1)=='/' and $dir=substr($dir,0,-1);
    return $dir;
}
/**
 * @static
 * 检查目录的可写入性，不可写入时，尝试变为可写入。
 * @param string $targetDir 目标目录
 * @return boolean
 */
function dirCheckWritable($targetDir){
    //检查缓存目录是否可写，不可写则修改它的属性
    if(!is_dir($targetDir))return false;
    elseif(!is_writable($targetDir) && !@chmod($targetDir,0777))return false;
    return true;
}
/**
 * @static
 * 检查目录是否存在
 * 当目录不存在时创建它
 * @param string $targetDir 目标目录,不支持相对目录
 * @return boolean
 */
function dirCheck($targetDir) {
    $targetDir = self::dirFormat($targetDir);
    if(is_dir($targetDir))return TRUE;
    $temp=$targetDir[0];
    if($temp=='.')return false;
    $tempDir=explode('/', $targetDir);
    $subDir=$tempDir[0];
    array_shift($tempDir);
    foreach ($tempDir as $value) {
        if($value=='') continue;
        if($value=='.' || $value=='..') return false;
        $subDir=$subDir.'/'.$value;
        if (is_dir($subDir)) continue;
        //创建目录
        if(!@mkdir($subDir, 0777) )return false;
    }
    return true;
}
/**
 * @static
 * 递归删除文件夹
 * @param string $targetDir 目标文件夹
 * @return boolean
 */
function dirDelete($targetDir) {
    if (!is_dir($targetDir)) return TRUE; //
    $handle = opendir($targetDir);
    if ($handle) {
        while (($item = readdir($handle)) !== false) {
            if ($item == '.' || $item == '..') continue;
            $itemFile = $targetDir . '/' . $item;
            if (is_dir($itemFile)) {
                dirDelete($itemFile);
                rmdir($itemFile);
            } else {
                unlink($itemFile);
            }
        }
        closedir($handle);
        rmdir($targetDir);
    }
    return TRUE;
}
/**
 * @static
 * 将一个文件夹内容，复制或移动到另一个文件夹
 * @param string $source 源文件夹名
 * @param string $target 目标文件夹
 * @param boolean $delete_source 是否删除源文件夹（是则相当于移动，否则相当于复制）
 * @return boolean
 */
function dirCopy($source, $target,$delete_source=FALSE) {
    $source=dirFormat($source);
    $target=dirFormat($target);
    if($source==$target) return TRUE;
    if(!is_dir($source)) return false;
    dirCheck($target);
    $handle = opendir($source);
    if(!$handle)return false;
    $source_path=$target_path='';
    while (($item = readdir($handle)) !== false) {
        if ($item == '.' || $item == '..') continue;
        $source_path=$source . '/' . $item;
        $target_path=$target.'/'.$item;
        if (is_dir($source_path)) {
            dirCopy($source_path, $target_path, $delete_source);
            if($delete_source ) rmdir($source_path);
        } else{
            copy($source_path, $target_path);
            if($delete_source ) unlink($source_path);
        }
    }
    closedir($handle);
    return true;
}


	/**
 * 创建目录,递归
 * */
	function Gmkdir($path,$qx='0777'){
		if(!is_dir($path)){
			$this->Gmkdir(dirname($path),$qx);
			mkdir($path,$qx);
		}
	}

	#实现目录遍历删除
	function del($filename){ 
		if(!is_dir($filename)){ 
			return FALSE; 
		} 
		 
		$arr=  glob($filename."/*"); 
		 
		foreach ($arr as $arr1){ 
			if(is_dir($arr1)){ 
				del($arr1);//删除目录 
			} 
			else{ 
				unlink($arr1);//删除文件 
			} 
		} 
		return rmdir($filename); 
	} 

/*


$file1 = 'test1.txt';
$str1 = file_get_contents($file1);
echo chkCode("$str1");
*/
	function chkCode($string){
 $code = array('UTF-8','GBK','GB18030','GB2312');
 foreach($code as $c){
  if( $string === iconv('UTF-8', $c, iconv($c, 'UTF-8', $string))){
   return $c;
  }
 }
 return "no";
}

	#fstat() -- 通过已打开的文件指针取得文件信息.
	function t()
	{
		$file = "php.txt";
		//打开文件，r表示以只读方式打开
		$handle = fopen($file,"r");
		//获取文件的统计信息
		$fstat = fstat($handle);
		echo "文件名：".basename($file)."<br>";
		//echo "文件大小：".round(filesize("$file")/1024,2)."kb<br>";
		echo "文件大小：".round($fstat["size"]/1024,2)."kb<br>";
		//echo "最后访问时间：".date("Y-m-d h:i:s",fileatime($file))."<br>";
		echo "最后访问时间：".date("Y-m-d h:i:s",$fstat["atime"])."<br>";
		//echo "最后修改时间：".date("Y-m-d h:i:s",filemtime($file))."<br>";
		echo "最后修改时间：".date("Y-m-d h:i:s",$fstat["mtime"]);

	}


}