<?php
/**
框架流程

加载核心类	z
加载CONFIG	config
初始化环境	init
加载安全	security
定义常量	const

加载
	control.class.php	控制器
	model.class.php		数据库操作类
	tpl.class.php		模板类
	error.class.php		捕捉错误异常等提醒
URL路由
	control	method
	route.class.php		根据URL,载入类

实例化当前类
spl_autoload_register(function ($name) {
    echo "111Want to load $name.\n";
  //  throw new MissingException("Unable to load $name.");
});
*/

/*

目录
core 主要MVC
ext	
help	第三方类库
*/
@date_default_timezone_set(@date_default_timezone_get());

define("ROOT", str_replace("\\", "/", dirname(__FILE__) ));
#路径, 基本路径
//!defined("BASEPATH") && define("BASEPATH",realpath(ROOT . '../').'/');



//define("APP_PATH", )
#核心类处理
require_once(ROOT ."/core/load.class.php");

//require_once("core/common.php");
#自动查找的目录
load::addDir(ROOT."/core");
load::addDir(ROOT."/ext");
load::addDir(ROOT."/config");
#常用公共函数引入
load::loadFile("common");
#程序执行时间,做个标记
load::loadClass('benchmark')->mark("total_exec_time");
#默认父 控制器
load::loadFile("control.class");

/**
核心类
require("kupe/kupe.php");

$KUPE = new kupe;
$KUPE->setApplication('app')->run();
*/
class kupe
{
	protected $applicationDir = './';
	
	public function __construct()
	{

	}
	public  function start()
	{
		return $this->run();
	}
	public  function run($myconfig = array())
	{


		#系统安全
		$this->security->init();



		#自动增加类库查找路径
		load::addDir(APPPATH . "/library");
		load::addDir(APPPATH . "/model");

		//load::loadFile("const",'config');
		
		//载入一个配置文件,将配置替换老的配置
		load::loadClass('config')
			->load(ROOT . "/config/global.conf.php")
			->load(APPPATH . "/data/config.php")
			->load($myconfig);
		
		#APP DIR
		//!defined("APPPATH") && define("APPPATH",realpath(BASEPATH . getconfig('application_name')).'/');
		#加载缓存类
		load::loadClass('cacheing', ROOT . '/core/cacheing/');


		#载入常量,xxxx
		//require_once(ROOT . "/config/const.php");
		#实例化模板引擎,并初始化

		//$this->tpl->setup();
		

		#构建路由,初始化
		$this->router()->init()->start();


		


		#生成常用的常量, __PATH 表示绝对路径, __ROOT__ 表示相对路径

		#网址根目录
		define('__ROOT__', pathinfo($_SERVER['PHP_SELF'], PATHINFO_DIRNAME ));

		#当前项目入口地址
		//define('__APP__', __ROOT__ . '/' .basename(APPPATH));
		define('__APP__', __ROOT__ );
		
		#DATA目录
		define('DATAPATH', APPPATH . "/data");
		
		#当前模板名称,当前模板路径
		define('TEMPLATE_NAME', getconfig('template_dir'));
		#当前模板皮肤目录
		define('TEMPLATE_PATH', APPPATH . "/".getconfig('dir_view')."/".TEMPLATE_NAME );

		
		#相对目录  SKIN 用于PHP端, __SKIN__用于前端
		define('SKIN', APPPATH . "/".getconfig('dir_view')."/".TEMPLATE_NAME );		
		define('__SKIN__', __APP__ . "/".getconfig('dir_view')."/".TEMPLATE_NAME );		
		
		

		define('__STATIC__', __SKIN__ . "/static");

		
		define('__JS__', __STATIC__ . "/js");
		define('__CSS__', __STATIC__ . "/css");
		define('__IMG__', __STATIC__ . "/images");

		
		#当前模块名
		define('MODULE_NAME', $this->router->c);
		#当前操作名
		define('ACTION_NAME', $this->router->m);
		
		define('NOW_TIME', time());

		#自动生成一些目录
		if(getconfig('kupe_status') == 'develop')
		{
			#项目时可以拿掉
			if(!is_dir(APPPATH.'/data'))
			{
				mkdir(APPPATH.'/data');
				mkdir(APPPATH.'/data/compile');
				mkdir(APPPATH.'/data/cache');
				mkdir(APPPATH.'/data/html');
				mkdir(APPPATH.'/data/sql');
			}
		
			load::loadClass("files")->dirCheck(__SKIN__);
			load::loadClass("files")->dirCheck(__STATIC__);
			load::loadClass("files")->dirCheck(__IMG__);
			load::loadClass("files")->dirCheck(__JS__);			
			load::loadClass("files")->dirCheck(__CSS__);
			
			
		}

		
		#生成常用的常量 / end

		#截入路由,根据当前的网址,载入app的控制器
		$c = new $this->router->c;
		$m = $this->router->method;


		
		#引用
		foreach(get_object_vars($this) as $k => $v)
		{
			$c->$k =& $v;
		}
		$c->$m();
		unset($c);
	}
	
	
    /**  PHP 5.1.0之后版本 */
    public function __isset($name) 
    {
        echo "Is '$name' set?\n";
        return isset($this->data[$name]);
    }

    /**  PHP 5.1.0之后版本 */
    public function __unset($name) 
    {
        echo "Unsetting '$name'\n";
        unset($this->data[$name]);
    }
	/*完成的工作DIR => 放弃*/
	function setApplicationDir($applicationDir = './')
	{	
		return $this;
	}

	public function __set($name, $value) 
    {
		if(load::is_loadclass($name))
		{
			 echo "<p style='background:#ccc;font-weight:bloder;color:red;'>注意:{$name} 已成为类名,您如果把它当变量使用,之前的类名将不可用</p>";
		}
        //$this->data[$name] = $value;
    }

	/*
	核心操作方法
	$this->input->xxxx();
	$this->output->xxxx();
	$this->db->xxxx();
	*/
    public function __get($name) 
    {
		return load::loadClass($name,null,null);
    }
   
	/*
	对方法重载
	针对核心类 直接 $this->文件名()->
	*/
	function __call($name,$b)
	{		
		return load::loadClass($name,null,$b);
	}
}