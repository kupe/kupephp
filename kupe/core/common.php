<?php
/**
公共函数库
*/

function p($v)
{
	load::loadClass("output")->p($v);
}
/**
返回实例
*/
function getInit()
{
	global $KUPE;
	return $KUPE;
}
/**
返回配置参数
*/

function getConfig($key = null)
{
	return load::loadClass("config")->getConfig($key);
}

function setConfig($key, $value)
{
	return load::loadClass("config")->setConfig($key, $value);
}

/**
	#实例化默认的数据库链接
	$db = load::loadModel('a');
	简化操作
*/
function m($table)
{	
	return load::loadModel($table);
}
