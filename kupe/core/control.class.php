<?php
/*
框架流程

加载核心类	z
加载CONFIG	config
初始化环境	init
加载安全	security
定义常量	const

加载
	control.class.php	控制器
	model.class.php		数据库操作类
	tpl.class.php		模板类
	error.class.php		捕捉错误异常等提醒
URL路由
	control	method
	route.class.php		根据URL,载入类

实例化当前类
*/
class control extends kupe
{
	function __construct()
	{
		//$this->test();	
	}
	//$sep
	public function display($file = null, $sep = '.')
	{
		$this->tpl()->setup();

		if(!$file)
		{
			$file = $this->router->c . $sep . $this->router->m . '.html';
		}

		
		
		$this->tpl->display($file);
	}


	
}