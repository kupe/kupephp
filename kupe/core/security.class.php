<?php
/*
框架流程

加载核心类	z
加载CONFIG	config
初始化环境	init
加载安全	security
定义常量	const


系统环境安全类,过滤一下
*/
class security
{
	public $http = array();
	public $get = array();
	public $post = array();
	function init()
	{

		foreach($_GET as $k => $v)
		{
			unset($_GET[$k]);
			//$_GET[$this->safeString($k)] = htmlspecialchars($v);
			//$_GET[$this->safeString($k)] = trim(addslashes(htmlentities($v)));
			//$_GET[$this->safeString($k)] = trim(addslashes(htmlspecialchars($v)));
			$_GET[$this->safeString($k)] = addslashes(trim($v));
		}
		
		foreach($_POST as $k => $v)
		{
			if(!is_array($_POST[$k]))
			{
				unset($_POST[$k]);				
			}
			//$_POST[$this->safeString($k)] = addslashes(htmlspecialchars($v));
			$_POST[$this->safeString($k)] = $this->addslashes2($v);
		}
		$this->get = $_GET;
		$this->post = $_POST;
		$this->http = array_merge($_GET,$_POST);
		return $this;
	}

	/**
	获取POST,GET的变量 , POST的优先级 > GET  
	security
	security
	se curi ty
	se curi ty
	*/
	public function getInput($key)
	{
		$this->get($key);
	}
	//改变一个值
	public function set($key, $value)
	{
		$this->http[$key] = $value;
		return true;
	}
	public function get($key)
	{
		if(!$key)
		{
			return $this->http;
		}
		if(isset($this->http[$key]))
		{
			return $this->http[$key];
		}
		return false;
	}
	##获取POST的数据
	public function post($key)
	{
		if(isset($this->post[$key]))
		{
			return $this->post[$key];
		}
		return false;
	}

	function safeString($str)
	{
		$str = preg_replace('/[^a-zA-Z0-9_%]/isU','',$str);
		return $str;
	}
	public function addslashes2($v)
	{
		if(is_array($v))
		{
			foreach($v as $kk => $vv)
			{
				$v[$kk] = $this->addslashes2($vv);
			}
		}
		elseif(is_string($v))
		{

			$v = addslashes(htmlspecialchars($v));
		}		
		return $v;
	}
}

