<?php
/**
配置类
对load.class.php有依赖关系
框架调用
load::loadClass('config')
			->load(ROOT . "config/global.conf.php")
			->load(BASEPATH . "{$this->application}/data/config.php");

*/
class config
{
	public $config = array();
	function __construct()
	{
		
	}
	/**
	载入一个配置文件,将配置替换老的配置
	*/
	function load($path)
	{		
		if(is_array($path))
		{
			$this->config = array_merge($this->config, $path);
		}
		elseif(is_file($path))
		{
			$this->config = array_merge($this->config, require_once($path));
		}
		//默认的数据库链接
		if(isset($this->config['db_host']))
		{
			$this->config['db0'] = array(
			'db_host' => $this->config['db_host'],
			'db_user' => $this->config['db_user'],
			'db_password' => $this->config['db_password'],
			'db_name' => $this->config['db_name'],	
			'db_charset' => $this->config['db_charset'],	
			'db_type' => $this->config['db_type'],	
			'db_port' => $this->config['db_port'],	
			'db_prefix' => $this->config['db_prefix'],	
			);
		}
		return $this;
	}
	function item($key)
	{
		if(isset($this->config[$key]))
		{
			return $this->config[$key];
		}
		return false;
	}
	## 支持 key . key 二维
	function getConfig($key = null)
	{
		if($key)
		{
			$tmp = explode('.', $key);
			if(isset($tmp[1]))
			{
				return $this->config[$tmp[0]][$tmp[1]];
			}
			else
			{
				return $this->item($key);
			}
		}
		return $this->config;
	}
	## 支持 key . key 二维
	function setConfig($key,$value)
	{
		$tmp = explode('.', $key);
		if(isset($tmp[1]))
		{
			$this->config[$tmp[0]][$tmp[1]] = $value;
		}
		else
		{
			$this->config[$key] = $value;
		}
		return $this;
	}


}