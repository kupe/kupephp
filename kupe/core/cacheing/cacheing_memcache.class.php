<?php

/**
用于缓存,目前只做了文件缓存

get()：通过一个指定的键（key）从缓存中取回一项数据。 如果该项数据不存在于缓存中或者已经过期/失效，则返回值 false。
set()：将一个由键指定的数据项存放到缓存中。
add()：如果缓存中未找到该键，则将指定数据存放到缓存中。
getOrSet()：返回由键指定的缓存项，或者执行回调函数，把函数的返回值用键来关联存储到缓存中，最后返回这个函数的返回值。
multiGet()：由指定的键获取多个缓存数据项。
multiSet()：一次存储多个数据项到缓存中，每个数据都由一个键来指明。
multiAdd()：一次存储多个数据项到缓存中，每个数据都由一个键来指明。如果某个键已经存在，则略过该数据项不缓存。
exists()：返回一个值，指明某个键是否存在于缓存中。
delete()：通过一个键，删除缓存中对应的值。
flush()：删除缓存中的所有数据。

*/
class cacheing_memcache 
{	
	public $path;
	public $m;
//	public $apptime;

	function __construct()
	{
		
		//$this->apptime = time();
	}
	function setup()
	{
		$config = getConfig('cacheing');
		#实例化
		$this->m = load::loadClass("memcache")->host($config['memcache']['host'])->port($config['memcache']['port'])->setup();
		return $this;
		
	}

	
	function getOrSet($key, $data)
	{
		$rs = $this->m->get($key);
	}
	
	
	function exists($key)
	{
		return $this->m->get($key);
	}

	function __call($func, $p)
	{
		if(method_exists($this->m, $func))
		{
			return call_user_func_array(array($this->m, $func), $p);
		}
		
	}
	/**function get($key, $expire = 0)
	{
		return $this->m->get($key, $expire);		
	}
	function set($key, $data, $expire = 0)
	{
		$this->m->set($key, $data, $expire);
	}
	function add($key, $data, $expire = 0)
	{
		$this->m->add($key, $data, $expire);
	}*/

	
	
}
