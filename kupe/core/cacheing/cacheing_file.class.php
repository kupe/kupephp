<?php

/**
用于缓存,目前只做了文件缓存

get()：通过一个指定的键（key）从缓存中取回一项数据。 如果该项数据不存在于缓存中或者已经过期/失效，则返回值 false。
set($key, $data, $expire = 0)：将一个由键指定的数据项存放到缓存中。
add()：如果缓存中未找到该键，则将指定数据存放到缓存中。
getOrSet()：返回由键指定的缓存项，或者执行回调函数，把函数的返回值用键来关联存储到缓存中，最后返回这个函数的返回值。
multiGet()：由指定的键获取多个缓存数据项。
multiSet()：一次存储多个数据项到缓存中，每个数据都由一个键来指明。
multiAdd()：一次存储多个数据项到缓存中，每个数据都由一个键来指明。如果某个键已经存在，则略过该数据项不缓存。
exists()：返回一个值，指明某个键是否存在于缓存中。
delete()：通过一个键，删除缓存中对应的值。
flush()：删除缓存中的所有数据。

*/
class cacheing_file 
{
	public $path;
	public $apptime;
	protected $prefix = '';

	function __construct()
	{
		$this->setup();
	}
	#文件缓存
	function setup()
	{
		$config = getConfig('cacheing');
		$this->path = $config['file']['path'];
		if(!is_dir($this->path))
		{
			mkdir($this->path);
		}
		$this->apptime = time();

		return $this;
	}
	function prefix($prefix)
	{
		$this->prefix = $prefix;
		return $this;
	}
	

	function get($key, $expire = 0)
	{
		if($this->exists($key))
		{
			$path = $this->getPath($key);
			$rs = require($path);
			#判断一下有效期
			if($rs['expire'] == 0)
			{
				return $rs['data'];
			}
			elseif($rs['expire'] > $this->apptime)
			{
				return $rs['data'];
			}
		}
		return false;
	}
	function getOrSet($key, $data)
	{
		$rs = $this->get($key);
	}
	function set($key, $data, $expire = 0)
	{
		$path = $this->getPath($key);
		return $this->writeVarToFile($path, $data, $expire);
	}
	function add($key, $data, $expire = 0)
	{
		$path = $this->getPath($key);
		return $this->writeVarToFile($path, $data, $expire);
	}
	
	/***/
	function getPath($key)
	{
		if(strlen($key) > 15)
		{
			$key = substr(md5($key), -16,16);
		}
		$path = $this->path . $this->prefix . $key . ".php";		
		return $path;
	}
	
	function multiGet($key, $data)
	{

	}
	function exists($key)
	{
		return file_exists($this->getPath($key));
	}
	function delete($key)
	{
		if($this->exists($key))
		{
			$path = $this->getPath($key);
			return unlink($path);
		}
		return false;		
	}
	/**
	返回删除缓存数,暂时支持一级目录缓存
	*/
	function flush()
	{
		$rs = glob($this->path . '*');
		$cnt = 0;
		foreach($rs as $k => $v)
		{
			if(is_file($v))
			{
				unlink($v);
				$cnt++;
			}
		}
		return $cnt;
	}
	//将变量写入文件
	function WriteVarToFile($path, $var,$expire = 0, $zip = false)
	{
		$var = array("expire" => $expire, 'data' => $var);

		$content = "<?php if( !defined(\"ROOT\") ){die(\"Hacking attempt\");}";
		$content .= 'return ' . var_export($var, true);
		if($zip)
		{
			//压缩缓存
			$content = preg_replace('/\s+/',' ',preg_replace('/\?>\s*<\?/','',$content));
		}	
		$content .= "; //Time:".date('Y-m-d H:i:s')." //BY http://www.kupebank.com\n";
		
		#验证目录,
		$this->Gmkdir(dirname($path));
		
		return file_put_contents($path, $content);
	}
	
	/**
 * 创建目录,递归
 * */
	function Gmkdir($path,$qx='0777'){
		if(!is_dir($path)){
			$this->Gmkdir(dirname($path),$qx);
			mkdir($path,$qx);
		}
	}
	
	/**
	 * @static
	 * 递归删除文件夹
	 * @param string $targetDir 目标文件夹
	 * @return boolean
	 */
	function dirDelete($targetDir) {
		if (!is_dir($targetDir)) return TRUE; //
		$handle = opendir($targetDir);
		if ($handle) {
			while (($item = readdir($handle)) !== false) {
				if ($item == '.' || $item == '..') continue;
				$itemFile = $targetDir . '/' . $item;
				if (is_dir($itemFile)) {
					$this->dirDelete($itemFile);
					rmdir($itemFile);
				} else {
					unlink($itemFile);
				}
			}
			closedir($handle);
			rmdir($targetDir);
		}
		return TRUE;
	}
	
}
