<?php

/**
程序性能记录类
用于记录框架 执行时间,执行SQL,载入文件数,等等统计
在CI的基础上扩展
	程序执行时间
	load::loadClass('benchmark')->mark("total_exec_time");
	load::loadClass('benchmark')->get_time("total_exec_time");
	程序所占用内存
	load::loadClass('benchmark')->memory_usage('k',3);
	载入文件数,依赖core/load.class.php类
	load::loadClass('benchmark')->require_total();
	执行SQL数,依赖core/db.class.php类
	load::loadClass('benchmark')->sql_total();

*/
class Benchmark {

	/**
	 * List of all benchmark markers
	 *
	 * @var	array
	 */
	public $marker = array();

	/**
	 * Set a benchmark marker
	 *
	 * Multiple calls to this function can be made so that several
	 * execution points can be timed.
	 *
	 * @param	string	$name	Marker name
	 * @return	void
	 */
	public function mark($name)
	{
		$this->marker[$name] = microtime(TRUE);
	}

	// --------------------------------------------------------------------

	/**
	 * Elapsed time
	 *
	 * Calculates the time difference between two marked points.
	 *
	 * If the first parameter is empty this function instead returns the
	 * {elapsed_time} pseudo-variable. This permits the full system
	 * execution time to be shown in a template. The output class will
	 * swap the real value for this variable.
	 *
	 * @param	string	$point1		A particular marked point
	 * @param	string	$point2		A particular marked point
	 * @param	int	$decimals	Number of decimal places
	 *
	 * @return	string	Calculated elapsed time on success,
	 *			an '{elapsed_string}' if $point1 is empty
	 *			or an empty string if $point1 is not found.
	 */
	public function elapsed_time($point1 = '', $point2 = '', $decimals = 4)
	{
		if ($point1 === '')
		{
			return '{elapsed_time}';
		}

		if ( ! isset($this->marker[$point1]))
		{
			return '';
		}

		if ( ! isset($this->marker[$point2]))
		{
			$this->marker[$point2] = microtime(TRUE);
		}

		return number_format($this->marker[$point2] - $this->marker[$point1], $decimals);
	}
	/**
	elapsed_time 的别名
	*/
	public function get_time($point,$decimals = 4)
	{
		return $this->elapsed_time($point, "{$point}_end", $decimals = 4);
	}

	// --------------------------------------------------------------------

	/**
	 * Memory Usage
	 *
	 *
	 * This permits it to be put it anywhere in a template
	 * without the memory being calculated until the end.
	 * The output class will swap the real value for this variable.
	 *
	 * @return	string	'{memory_usage}'
	 */
	public function memory_usage($unit = 'k',$decimals = 3)
	{
		if($unit == 'm')
		{
			$memory = memory_get_usage()/(1024*1024);			
		}
		else
		{
			$memory = memory_get_usage()/1024;
		}
		return round($memory,$decimals);
		#return '{memory_usage}';
	}
	/**
	real_usage
	如果设置为 TRUE，获取系统分配的真实内存尺寸。如果未设置或者设置为 FALSE，将是 emalloc() 报告使用的内存量。 
	*/
	public function memory_total($unit = 'k',$decimals = 4)
	{
		if($unit == 'm')
		{
			$memory = memory_get_usage(TRUE)/(1024*1024);			
		}
		else
		{
			$memory = memory_get_usage(TRUE)/1024;
		}
		return round($memory,$decimals);
		#return '{memory_usage}';
	}

	/**
	载入文件数,依赖core/load.class.php类
	*/
	public function require_total()
	{
		return count(load::is_loadfile());
	}
}
