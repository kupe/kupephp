<?php
defined('ROOT') OR exit('No direct script access allowed');

/**
载入类,
对所有载入的文件,类进行统一管理
此类只静态使用,不做实例化
依懒关系尽量做到最小
使用方法
*/
class load
{
	/*
	载入类,返回类的实例
	$fileName 文件名
	$dir 所在目录,
	$par 实例化时带的参数
	*/
	static public $is_load_file = array();
	static public $__classDir = array();
	static public $__callClass = array();
	static public $__callModel = array();
	static public $__dbConnect = array();
	#总执行SQL的次数
	static public $APP_RUN_SQL = 0;
	static public $APP_RUN_SQLS = array();
	/**
	$dir 额外查找的目录,而且优先查找
	*/
	static function loadFile($fileName,$dir = null)
	{
		$dirs = self::$__classDir;
		if($dir)
		{
			$dirs = array_merge(array($dir), $dirs);
		}
		foreach($dirs as $k => $v)
		{
			$path = realpath( "{$v}/{$fileName}.php");				
			if(is_file($path))
			{
				self::is_loadfile($path);
				return include_once($path);
				break;
			}
		}
		load::loadClass('help')->show("{$path}无法载入文件:{$fileName}");
		return false;
	}
	/**
	载入类,返回类的实例
	$fileName 文件名
	$dir 所在目录,
	$b 实例化时带的参数
	*/
	static function  loadClass($className,$dir = null,$b = null)
	{
		
		if(isset(self::$__callClass[$className]))
		{
			//echo $className . "<br>";
			return self::$__callClass[$className];
		}
		#有待改进  =>  call_user_func_array(array($foo, "bar"), array("three", "four"));
		if(!$b)
		{
			$b = array(0=>null,1=>null);
		}
		$dirs = self::$__classDir;
		if($dir)
		{
			$dirs[] = $dir;
		}
		
		foreach($dirs as $k => $v)
		{
			$path = realpath("{$v}/{$className}.class.php");

			
			
			if(is_file($path))
			{
				#EXT 目录内的使用区别与其它的类名
				if($v == ROOT."/ext")
				{
					$className = 'EXT_' . $className; 
				}
				
				
			
				include_once($path);
				//记录已经载入过的类名
				self::is_loadclass($className);
				#记录所有载入的文件名
				//self::is_loadfile("{$className}.class");
				self::is_loadfile($path);
				self::$__callClass[$className] = new $className($b[0]);
				//self::$__callClass[$className] = call_user_func_array(array($foo, "bar"), array("three", "four"));

				break;
			}
			else
			{
				//echo ("{$v}/{$className}.class.php". "   不存在<br>");				
			}
		}
		if(isset(self::$__callClass[$className]))
		{
			return self::$__callClass[$className];
		}
		p($dirs);
	    throw new Exception("无法载入: <h1>$className</h1> {$path} ");
		return true;
		
	}
	static function loadModel($model_name,$server_id = 0)
	{
		if(isset(self::$__callModel[$model_name]))
		{
			return self::$__callModel[$model_name];
		}
		load::loadClass("db");		

		$path = APPPATH. "/model/{$model_name}.model.php";
		if(is_file($path))
		{
			self::is_loadfile($path);
			require_once($path);
		}
		else
		{
			if(getconfig('kupe_status') == 'develop')
			{
				load::loadClass('help')->show("开发模式,已自动创建:{$path}");
				load::loadClass('help')->makeModel($model_name);	exit;			
			
			}
			exit("找不到{$path}模型");
		}
		
		$domodel = "{$model_name}model";
		$obj = new $domodel($model_name,$server_id);

		#设置默认表名
		/*
		if (!isset($obj->table) || empty($obj->table))
		{
			#表示为小写
			$obj->table = $model_name;
			$tmp = getconfig('db'.$server_id);
			$obj->prefix = $tmp['db_prefix'];
			$obj->doTable = $obj->prefix . $obj->table;
			$obj->server_id = $server_id;
		}
		#设置一个默认主键
		if (!isset($obj->pk))
		{
			$obj->pk = 'id';
		}
		*/
		self::$__callModel[$model_name] = $obj;
		return $obj;
		
		echo "写到:load.class.php {loadModel}";
	}

	/**
	
	 * @param	string
	 * @return	array
	 */
	static function &is_loadfile($path = '')
	{
		static $_is_loaded = array();

		if ($path !== '')
		{
			$_is_loaded[strtolower($path)] = $path;
		}
		return $_is_loaded;
	}
	/**
	 * Keeps track of which libraries have been loaded. This function is
	 * called by the load_class() function above
	 *
	 * @param	string
	 * @return	array
	 */
	static function &is_loadclass($class = '')
	{
		static $_is_loaded = array();

		if ($class !== '')
		{
			$_is_loaded[strtolower($class)] = $class;
		}
		return $_is_loaded;
	}
	static public function addDir($classDir)
	{		
		self::$__classDir[] = $classDir;
		return self::$__classDir;
	}
	static public function getDir()
	{
		return self::$__classDir;
	}
	/**
	显示框架全局的信息
	*/
	static public function info()
	{
		$APP_RUN_SQL = self::$APP_RUN_SQL;
		$msg[] = "总执行SQL次数:{$APP_RUN_SQL}次";
		

		$msg[] = "<span>自动查找类的目录列表:</span>";
		foreach(self::$__classDir as $k => $v)
		{
			$msg[] = "{$v}";
		}

		$msg[] = "<span>总载入的类有:</span>";
		foreach(self::$__callClass as $k => $v)
		{
			$msg[] = "{$k}";
		}

		$msg[] = "<span>总载入的MODEL有:</span>";
		foreach(self::$__callModel as $k => $v)
		{
			$msg[] = "{$k}";
		}
		$msg[] = "<span>__dbConnect 链接数据库:</span>". count(self::$__dbConnect) . "个";

		$msg[] = "<span>共载入文件数:". count(get_included_files()). "个</span>";
		foreach(get_included_files() as $k => $v)
		{
			$msg[] = "{$k} {$v}";
		}

		//p(self::is_loadfile());
		
		$msg[] = "<span>内存占用:".load::loadClass('benchmark')->memory_usage()."</span>";
		$msg[] = "<span>执行时间:".load::loadClass('benchmark')->get_time('total_exec_time')."S</span>";


		self::loadClass("help")->show(implode("<br>\n",$msg));
	}

}