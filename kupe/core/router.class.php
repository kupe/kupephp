<?php
/**
核心:路由类
使用方法:
#构建路由,初始化
	$this->router()->application("app")->init();
	#截入路由,根据当前的网址,载入app的控制器
	$this->router()->start();	
OR 
	#构建路由,初始化,启动,一步完成
	$this->router()->application("app")->init()->start();


$c = new $this->router->c;
$m = $this->router->m . EXTEND_M;
#引用
foreach(get_object_vars($this) as $k => $v)
{
	$c->$k =& $v;
}
$c->$m();

变量说明
$this->c	当前control
$this->m	当前method
$this->urlmod	网址模式.  path,rewrite,url

依赖 input  ?
*/
class router
{
	//$method 方法全名
	public $c,$m,$method,$application = '';
	public $urlmode;
	public $config;
	function __construct($config = array())
	{
		$this->config = getconfig('url');
	}
	function vars($key)
	{
		return $_POST[$key] ? $_POST[$key] : $_GET[$key];
	}
	function init()
	{
		$http = load::loadClass('security')->http;
		
		
		$this->c = isset($http[$this->config['c']]) && $http[$this->config['c']] ? $http[$this->config['c']] : $this->config['default_c'];
		$this->m = isset($http[$this->config['m']]) && $http[$this->config['m']] ? $http[$this->config['m']] : $this->config['default_m'];
		return $this;
	}
	#载入具体的control
	function start()
	{		
		$path = APPPATH  . '/'. getconfig('control_dir') ."/" . $this->c . ".class.php";
		if(is_file($path))
		{
			include_once($path);
		}
		else
		{			
			if(getconfig('kupe_status') == 'develop')
			{
				load::loadClass('help')->show("{$path}不存在.<br>由于开发模式,已自动创建:{$path}");
				load::loadClass('help')->makeContrl($this->c,$path);	exit;	
			}
			else
			{
				$this->show("{$path}不存在");
			}
			return false;
		}
		if(!class_exists($this->c))
		{
			$this->show("className:\"{$this->c}\" 不存在");
			return false;
		}
		$this->method = $m = $this->m . $this->config['method_prefix'];

		if(!method_exists($this->c,$m))
		{
			$this->show(nl2br("{$this->c}->{$m}()方法不存在,创建方法
			/**
			功能说明:	
			创建日期:".date('Y-m-d')."
			*/
			public function {$m}()
			{

			}			
			"));exit;
			return false;
		}
	}
	/**
	按要求返回URL
	*/
	static function url($c = 'index',$m = 'index',$par = null)
	{		
		!$c && $c = getconfig('url.default_c');
		!$m && $m = getconfig('url.default_m');
		/*
		$script_name =  dirname($_SERVER['SCRIPT_NAME']) . '/';
		if ($script_name == './')
		{
			$script_name = '/';
		}
		else
		{
			$script_name = str_replace('//', '/', $script_name);
		}
		*/
		$url = $_SERVER['SCRIPT_NAME'];
		
	
		
		if(is_array($par) && isset($par[0]))
		{
			$tmp = explode('/', $par[0]);
			$c = $tmp[0];
			$m = $tmp[1];
		}
		$url .= "?". getconfig('url.c') . "={$c}&".getconfig('url.m')."={$m}";
		
	

		if(is_null($par))
		{

		}
		elseif(is_array($par))
		{
			unset($par[0]);
			foreach($par as $k => $v)
			{
				$url .= "&{$k}=".$v;
			}			
		}
		elseif(is_string($par))
		{
			$url .= $par;
		}
		return $url;

	}
	/** 返回URL  这个函数有点BUG, BY 边缘狂人*/
	static function _url($m,$par = null)
	{
		return self::url($this->c, $m,$par);
	}

	/**
	使SMARTY模板支持 URL函数
	*/
	static function _tpl_url($v)
	{
		if (isset($v[getconfig('url.c')]))
		{
			$c = $v[getconfig('url.c')];
			unset($v[getconfig('url.c')]);
		}
		else
		{
			$c = '';
		}
		$_m = getconfig('url.m');
		if (isset($v[$_m]))
		{
			$m = $v[$_m];
			unset($v[$_m]);
		}
		else
		{
			$m = 'index';
		}
		
		return self::url($c, $m, $v);
	}
	/*
	使SMARTY模板支持 _URL函数
	*/
	static function _tpl__url($v)
	{
		$c = load::loadClass('router')->c;
		if (isset($v[getconfig('url.m')]))
		{
			$m = $v[getconfig('url.m')];
			unset($v[getconfig('url.m')]);
		}
		else
		{
			$m = '';
		}
		return self::url($c, $m, $v);
	}
	function _tpl_c($v)
	{
		return self::C($v['key']);
	}

	function show($msg)
	{
		load::loadclass("help")->show($msg);
	}

	##################################################################################
	##################################################################################
	##################################################################################
	#可被重写
	function succeed($msg,$jumpurl="",$t=1)
	{
		if ($jumpurl)
		{
			$js = "<script>alert('{$msg}');self.location='{$jumpurl}'</script>";
		}
		else
		{
			$js = "<script>alert('{$msg}');history.back();</script>";
		}
		echo $js;
		exit();
		if($jumpurl == "")
		{
			$jumpurl = "javascript:history.back()";
		}
		$ifjump = "<META HTTP-EQUIV='Refresh' CONTENT='$t; URL=$jumpurl'>";
		include_once(D_P . 'template/' .SKIN . '/message.tpl.html');
		exit;
	}
	#可被重写
	function error($msg, $jumpurl = '')
	{
		if ($jumpurl)
		{
			$js = "<script>alert('{$msg}');self.location='{$jumpurl}'</script>";
		}
		else
		{
			$js = "<script>alert('{$msg}');history.back();</script>";
		}
		header('content-type:text/html;charset=utf-8');

		echo $js;
		exit();
	}
	#可被重写
	function jump($jumpurl)
	{
		//header('Location:'. $jumpurl);exit;
		echo "<a id='b3' href='{$jumpurl}' style='color:#fff;'>&nbsp&nbsp&nbsp</a>";
		echo '<script language="javascript">
function dddte(){   
   if(document.all){document.getElementById("b3").click(); }         
	else { 
		var evt = document.createEvent("MouseEvents"); 
		evt.initEvent("click",true,true); 
		document.getElementById("b3").dispatchEvent(evt); 
	}   
} dddte(1);
</script>';
		echo "<script>function go(){self.location='{$jumpurl}'} setTimeout('go()', 1000)</script>";
		exit;
	}
	/*
	通过GET方式,跳转到某地址
	*/
	function getGo($url)
	{
		//error_reporting(E_ALL);
		#分解URL
		preg_match_all('/(\w+)=(\w+)/', $url, $p);
		$to = preg_replace('/\?.*/', '', $url);
		echo "<form action='{$to}' method='get' id='postgo'>";
		foreach ($p[1] as $k => $v)
		{
			echo "<input type='hidden' name='{$v}' value='{$p[2][$k]}'>";
		}
		echo "</form><script>document.getElementById('postgo').submit();</script>";

		exit;
	}
	function confirm($msg,$url1,$url2 = '')
	{
		if($url1)
		{
			$url1 = "self.location.href='{$url1}';";
		}
		else
		{
			$url1 = "history.back(-1);";
		}
		if($url2)
		{
			$url2 = "self.location.href='{$url2}';";
		}
		else
		{
			$url2 = "history.back(-1);";
		}	
		return "<script>
				if(confirm('{$msg}'))
				{
					{$url1}
				}
				else
				{
					{$url2}
				}
				</script>";
	}
	function _sql($table,$arr)
	{
		$sql = "CREATE TABLE  `{$table}` (
`id` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY 
";

		foreach($arr as $k => $v)
		{
			$sql .= ",`{$k}` VARCHAR( 50 ) NOT NULL COMMENT  '{$v}'";
		}
		$sql .= ") ENGINE = MYISAM";
		p($sql);
	}
}