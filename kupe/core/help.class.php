<?php
/**
自动获取框架中类的帮助说明
调用方法,在框架中
$this->help->auto('help');

*/
class help
{
	public $lang = array(
		
	);
	
	function __call($name,$b)
	{
		if(isset($this->lang[$name]))
		{
			$this->showHelp($this->lang[$name]);
		}
		else
		{
			$this->showHelp("找不到关于 {$name} 的帮助信息");
		}
	}
	//返回指定类中的方法 内的参数
	//return array()
	function getMethodP($className,$method)
	{
		$rs = array();
		$a = new ReflectionMethod($className,$method);

		$attr = Reflection::getModifierNames($a->getModifiers ());
		$tostring = $a->__toString ();
		if($tostring)
		{
			//p(current(explode("@@",$tostring)));exit;
			$tostring = current(explode("Method",$tostring));
		}
		

		//$tostring = $a->export($className,$method,true);
		foreach($a->getParameters() as $k => $v)
		{
			$rs[] = $v->name;
		}
		return array('attr' => $attr[0], 'rs' => $rs, 'tostring' => $tostring);
	}
	/*function auto($obj)
	{
		if(!is_object($obj))
		{
			$this->showHelp("<div class='alert alert-danger-inverse with-icon'>
			<i class=\"icon-question\"></i>
			<div class='content'>错误:您输入的不是类对象,无法获取帮助信息</div>
			</div>");
			exit;
		}
		$className = get_class($obj);
		$lang = $this->getLang($className);

		$msgs = '';
		$msgs .= "<div class='col-md-12'>";		
		$msgs .= "<h1 class=\"alert\">代码提示:{$className} 类用途</h1>";
		$msgs .= "</div>";	

		$msgs .= "<div class='col-md-12'>";
		foreach(get_object_vars($obj) as $k => $v)
		{
			$msgs .= "	<div class='btn'><h3>变量 \${$k};</h3></div>";
		}
		$msgs .= "</div>";	
		foreach(get_class_methods($obj) as $k => $v)
		{
			if(isset($lang[strtolower($v)]))
			{
				$content = $lang[strtolower($v)];
			}
			else
			{
				$content = "描述未添加";
			}
			$a = $this->getMethodP($className, $v);
			

			$content = nl2br($content);

			$msgs .= "<div class='col-md-6'>";
			$msgs .= "
			<div class='alert alert-info-inverse with-icon'>
			
			 <i class=\"icon-file-code\"></i>
			 <div class='content'>
				<h3>function {$v}(".implode(',',$a).")</h3>
				
				<blockquote >
				{$content}
				</blockquote></div>
			</div>
			";
			$msgs .= "</div>";	
		}
		$this->showHelp($msgs);
	}*/
	function readme($className)
	{
		return $this->auto($className);
	}
	/**返回类的信息*/
	function read($className)
	{
		$rc = new ReflectionClass($className);
		#获取类中的注释,方法,变量
		$comment = $rc->getDocComment();
		$methods = $rc->getMethods();
		$rec = array();
		foreach($methods as $k => $v)
		{
			$method_name = strtolower($v->name);
			$a = $this->getMethodP($className, $method_name);
			$a['method'] = $method_name;
			$a['class'] = $v->class;
			$rec[] = $a;
		}
		return $rec;

	}
	function auto($className)
	{		
		if(!class_exists($className))
		{
			$this->showHelp("<div class='alert alert-danger-inverse with-icon'>
			<i class=\"icon-question\"></i>
			<div class='content'><h2>错误:您输入的是:{$className}不是类对象,无法获取帮助信息</h2></div>
			</div>");
			exit;
		}
		$rc = new ReflectionClass($className);
		#获取类中的注释,方法,变量
		$comment = $rc->getDocComment();
		$methods = $rc->getMethods();
		$vars = $rc->getDefaultProperties();
		$fileName = $rc->getFileName();

		unset($rc);


		//exit;

		$lang = $this->getLang($className);

		$msgs = '<div class="row">';
			$msgs .= "<div class='col-md-12'>";		
			$msgs .= "<h1 class=\"alert\">代码提示:{$className} 类帮助说明</h1>";
			$msgs .= "</div>";	

			

			$msgs .= "<div class='col-md-12'>";
			$msgs .= "<div><pre><p>文件路径:{$fileName}</p><code>{$comment}</code></pre>";	

			$msgs .= "</div>";
		//row end
		$msgs .= "</div>";	

		$msgs .= '<div class="container"><div class="row">';


		$msgs .= "<div class='col-md-12'>";
		foreach($vars as $var => $value)
		{
			$msgs .= "	<div class='btn'><h3>变量 \$this->{$var};</h3></div>";
		}
		$msgs .= "</div>";	
		$msgs .= "</div></div><!--row end-->";	

		$msgs .= '<div class="container"><div class="row">';
		foreach($methods as $k => $v)
		{
			$method_name = strtolower($v->name);
			$a = $this->getMethodP($className, $method_name);


			if(isset($lang[$method_name]))
			{
				$content = $lang[$method_name] . "<br>{$a['tostring']}";
			}
			else
			{
				$content = $a['tostring'];
			}
			
			$content = nl2br($content);

			$msgs .= "<div class='col-md-6'>";
			$msgs .= "
			<div class='alert alert-info-inverse with-icon'>
			
			 <i class=\"icon-file-code\"></i>
			 <div class='content'>
				<h3>{$a['attr']} function {$v->name}(".implode(',',$a['rs']).")</h3>
				
				<blockquote >
				<p>{$content}</p>
				</blockquote>
				
				</div>
			</div>
			";
			$msgs .= "</div>";	
		}
		$msgs .= "</div></div><!--row end-->";	
		$this->showHelp($msgs);

	}
	/*
	获取帮助语言包
	*/
	function getLang($className)
	{		
		$defaultLang = array(
		'__construct'	 => '构造函数:类实例化时被执行',
		'__destruct'	 => '析构函数会在到某个对象的所有引用都被删除或者当对象被显式销毁时执行',
		'__call'	 => '当调用类中没有的方法时,会被调用',
		'__get'	 => '',
		'__get'	 => '',
		'error'	 => '',
		'application'	 => '设置实例程序的目录',
		);
		$className = strtolower($className);
		$langs = parse_ini_file(ROOT . "config/help.conf",true);
		if(isset($langs[$className]))
		{
			#将配置中的KEY统一转成小写,方便兼容
			foreach($langs[$className] as $k => $v)
			{
				$rs[strtolower($k)] = $v;
			}
			return array_merge($defaultLang,$rs);
		}
		return $defaultLang;

	}



	function show($content,$title = "信息提示")
	{
		$msg = '';
		
		$msg .= "<style>
		.error{
		border-bottom:1px solid #ccc;
		border-right:1px solid #ccc;
		line-height:30px;
		margin-left:300px;
		margin-right:300px;
		margin-top:50px;
	}

		.error .footer{
		text-align:right;
	}
		.error div{
		border-top:1px solid #ccc;
		border-left:1px solid #ccc;
		padding:10px;

	}
		.error .header{
		font-weight:bolder;
		background:#ddd;
	}
		</style>";
		$msg .= "<div class='error'>
					<div class='header'>{$title}:</div>";

		$msg .= $content;
		
		$msg .= "\n<div class='footer'><a href='http://www.kupebank.com'>来自:Kupe框架</a></div></div>";
		echo $msg;


	}

	/*
	显示帮助信息 ,$status = 1
	*/
	function showHelp($content)
	{
		/*if(is_array($msg))
		{
			$content = implode("<br>\n",$msg);
		}
		else
		{
			$content = $msg . "<br>\n";
		}
		$content = $msg;*/

		print <<<EOT


	<!DOCTYPE html>
<html lang="zh-cn">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>代码提示</title>
    <!-- zui -->
   			<!-- ZUI 标准版压缩后的 CSS 文件 -->
	<link rel="stylesheet" href="//cdn.bootcss.com/zui/1.7.0/css/zui.min.css">

	<!-- ZUI Javascript 依赖 jQuery -->
	<script src="//cdn.bootcss.com/zui/1.7.0/lib/jquery/jquery.js"></script>
	<!-- ZUI 标准版压缩后的 JavaScript 文件 -->
	<script src="//cdn.bootcss.com/zui/1.7.0/js/zui.min.js"></script>
	<!-- zui END -->
  </head>
  <body>
		<div class="container">
		<!-- 在此处编码你的创意 -->
			{$content}
		</div>

   
  </body>
</html>
EOT;

		
	}

	/**
	自动生成 model.class.php 文件
	*/
	function makeModel($modelName)
	{
		$path = APPPATH. "model/{$modelName}.model.php";
		if(is_file($path))
		{
			return true;
		}
		$content = "<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : ".date('Y-m-d H:i:s')."
#path : $path
*/
class {$modelName}Model extends db
{

}";
		$content = str_replace(BASEPATH,'', $content);
		return file_put_contents($path,$content);
		
	}
	/**
	自动生成模型 model.class.php 文件
	只有在开发模式下才执行
	*/
	function makeContrl($c,$path)
	{
		//$path = BASEPATH . getconfig('application_name'). "/model/{$modelName}.class.php";
		load::loadClass('files')->gmkdir(dirname($path));
		if(is_file($path))
		{
			return true;
		}
		//load::info();

		
		$content = "<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : ".date('Y-m-d H:i:s')."
#path : $path
*/
class {$c} extends control
{
	/** 
		Home
	*/
	public function indexAction()
	{
		echo \"Hello WORD\";
	}
	/** */
	public function addAction()
	{

	}
	/** */
	public function doaddAction()
	{

	}
	/** */
	public function editAction()
	{

	}
	/** */
	public function doeditAction()
	{

	}
	/** */
	public function deleteAction()
	{

	}
}";
		$content = str_replace(APPPATH,'', $content);
		file_put_contents($path,$content);

		
		if(!is_dir(APPPATH . "/data"))
		{
			
			#生成标准目录
			load::loadClass('files')->gmkdir(APPPATH . "/data");
			load::loadClass('files')->gmkdir(APPPATH . "/data/compile");
			load::loadClass('files')->gmkdir(APPPATH . "/data/cache");
			load::loadClass('files')->gmkdir(APPPATH . "/data/html");
			load::loadClass('files')->gmkdir(APPPATH . "/data/sql");

			load::loadClass('files')->gmkdir(APPPATH . "/mode");
			load::loadClass('files')->gmkdir(APPPATH . "/template/default");

		}

		
	}
	/**
	自动创建模板文件
	*/
	function makeTemplate($path)
	{
		
		load::loadClass('files')->gmkdir(dirname($path));
		if(is_file($path))
		{
			return true;
		}
		//load::info();
		$content = "<!---
@自动生成 BY Kupe框架 www.kupebank.com
#date : ".date('Y-m-d H:i:s')."
#path : $path
--->  Hello WORD";
		//$content = str_replace(BASEPATH,'', $content);
		return file_put_contents($path,$content);

		echo $path;
	}
}