/*
JSON.stringify()

//var $ = layui.$;
*/

layui.use(['element', 'table','form','layer','laypage', 'layedit','upload'], function(){
  var element = layui.element,
  $ = layui.jquery,
  table = layui.table,
  form = layui.form,
  laypage = layui.laypage,
  layedit = layui.layedit,
  upload = layui.upload,
  element = layui.element,
  layer = layui.layer;


	/*$.each($('.n'),function(){
		$(this).mouseover(function(){
			$('.n').parent().removeClass('layui-nav-itemed');
			$(this).next("dl").show(100);
			$(this).parent().addClass('layui-nav-itemed');

		});
	});*/

	/**
	用于移动排序
	$("#main_table_id").dragsort({ dragSelector: "tr", dragEnd: function(){
		 $('#result').html("移动了");saveOrder();
		
		 }, placeHolderTemplate: "<tr><td></td></tr>",dragSelectorExclude:'input,textarea,a,select,label,th' });

	*/
		
  
});

/*分类中增加文章数*/
function articleCount(_name,_id,_v){
	 $ = layui.jquery ; 

	
	$('[name="'+_name+'"] option[value="'+_id+'"]').text('9999999999');
	//$('[name="'+_name+'"] option[value="'+_id+'"]').text($('[name="'+_name+'"] option[value="'+_id+'"]').text()+"("+_v+")");
}



      

//$ = layui.$ ;
//打开一个页面
function openPage(url,title){
		layui.$.get(url,function(str){
				
				  layer.open({
					type: 1,
					shadeClose:1,
					title:title,
					maxWidth:500,
					area:500,
					scrollbar : false,
					//shade:1,
					content: str 
				  });

			});
	}

function gjson(d){
	if(d.substr(0,1) != '{'){
		/*layui.$('body').before(d);*/
		alert(d);
		layui.layer.msg(d);
	}
	d = layui.$.parseJSON(d);	
	return d;
}


function myconfirm(url,title = "您确认要删除吗?"){

	layer.confirm(title, {icon:6,shadeClose:1,}, 
	function(index){
		//url = '{_url m="dodel" id=''}'+id+"&t="+Math.random();		
		layui.$.get(url,function(str){
			layer.close(index);
			d = gjson(str);
			if(d.status == 1){
				layer.msg(d.msg,{icon: 6,time:1000},function(){
					self.location.reload();
				});			
			}else{
				layer.msg(d.msg);
			}	
		});
	},
	function(index){  layer.close(index);}
	);
}

/*
手册:

	table.on('edit(main_table_)', function(obj){ //注：edit是固定事件名，test是table原始容器的属性 lay-filter="对应的值"
		 console.log(obj.value); //得到修改后的值
	  alert(obj.data.id);
	  console.log(obj.field); //当前编辑的字段名
	  console.log(obj.data); //所在行的所有相关数据 

		
	});
--------------

必须有一个ID字段,用于保存数据的查询条件
doFastEdit('{_url m="fast"}', 'main_table');
filter
*/
function doFastEdit(url,filter){

	layui.table.on('edit('+filter+')', function(obj){
			var value = obj.value //得到修改后的值
			,data = obj.data //得到所在行所有键值
			,field = obj.field; //得到字段
			layui.$.post(url, {'id':data.id, 'field':field, 'value':value},function(str){			
				d = gjson(str);
				if(d.status == 1){
					layer.msg(d.msg,{icon: 6,time:1000},function(){
						/*self.location.reload();
						tableIns.reload();*/						 
					});			
				}else{
					layer.msg(d.msg);
				}	
			});
			/*layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改为：'+ value);*/
		});
}
function fastEdit2(url,filter){
	$ = layui.$;
	$.each($('.'+filter),function(){		
		$(this).click(function(){
			if($(this).html().substring(0,1) != '<'){
				$(this).html($(this).children().val());
				$(this).html("<input class='layui-input layui-input-sm' style='height:auto;' type='text' value='" + $(this).html() + "'>");	
				$(this).children().focus();
			}
		});
		$(this).change(function(){
			
			id = $(this).parent().attr('dataid');
			field = $(this).attr('datafield');
			value = $(this).children().val();


			$.get(url,{'id':id,'field':field,'value':value},function(str){
				d = gjson(str);
				if(d.status == 1){
					layer.msg(d.msg,{icon: 6,time:1000},function(){
						/*self.location.reload();
						tableIns.reload();*/						 
					});			
				}else{
					layer.msg(d.msg);
				}	
			});
			$(this).html($(this).children().val());
			
		});
		$(this).focusout(function(){
			$(this).html($(this).children().val());
		});


	});
	
}
/*
针对表单的提交,使用

formSubmit('{_url m="doedit"}','formDemo');
*/
function formSubmit(url,filter,after = 'reload'){

	
	layui.form.on('submit('+filter+')', function(data){

		$ = layui.$		
		layui.$.post(url,data.field,function(str){	
			/*d = layui.$.parseJSON(str);*/
			d = gjson(str);
			if(d.status == 1){
				layui.layer.msg(d.msg,{icon: 6,time:1500},function(){					
					if('reload' == after){
						self.location.reload();
					}else if('close' == after){
						//a = alert(layui.layer.index);
						//alert(a);
						//layui.layer.close(layui.layer.index-1);
						layui.layer.close(layui.layer.index-1);
						//layui.layer.closeAll();
					}else if('comfirm:' == after.substring(0,8)){
						url = after.substring(8);
						layer.confirm("操作成功,是否返回上一页", {icon:6,shadeClose:1,}, function(){
							self.location.href=url;
						},function(){ /*  暂时没有办法,只好刷新处理
						*/self.location.reload();return false;});
					}else if('go:' == after.substring(0,3)){
						url = after.substring(3);
						self.location.href=url;
					}else if('reset' == after){

						if(confirm("操作成功,是否继续添加?")){
							$('#open_page_form')[0].reset();
						}else{
							self.location.reload();
						}

					}else if('url:' == after.substring(0,4)){
						url = after.substring(4);						
						layui.$.get(url,function(str){
							$('#main_table_id').html(str);
						});
						layui.layer.close(layui.layer.index-1);						
					}
					
				});			
			}else{
				layui.layer.msg(d.msg);
			}		
		});
		return false;
	});
	layui.form.render();
	return false;
}


layui.use('util', function(){
  var util = layui.util;
  
  //执行
  util.fixbar({
    bar1: true
    ,click: function(type){
      console.log(type);
      if(type === 'bar1'){
       // alert('点击了bar1')
      }
    }
  });
});




		function showAlert(msg){
			layui.layer.alert(msg,{icon: 6,shadeClose :1});
			return false;
		}