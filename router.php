<?php
/*
这是一个调度页

URL规律
*.html  查导航,分类页

index.html		首页
{name2}.html	分类页
{name2}-数字(1,2,3,4).html	模型的分类

模型tag-数字.html  具体内容页
*/
//header("location:admin.php?c=index&m=login&user=demo&password=demo");exit;



define("APPPATH", dirname(__FILE__) . '/');


require_once("kupe/kupe.php");

$key = $_SERVER['REQUEST_URI'];
$key = reset(@explode(".html",basename($key)));
if(!file_exists('data/cache/global.php'))
{
	require("index.php");exit;
}
$router = require("data/cache/global.php");
$router = $router['data'];

$router = $router['router'];



#自定义别名,包括 分类页,有自定义网址的内容页
if(isset($router[$key]))
{
	//	echo $router[$key];
	parse_str($router[$key], $get);
	foreach($get as $k => $v)
	{
		$_GET[$k] = $v;
	}
}
#具体内容页面,没有自定义网址的页面
elseif(isset($_GET['modetag']) && isset($_GET['id']))
{
	//具体内容页
	$get = ['m' => 'content', 'mode' => $_GET['modetag']];
	foreach($get as $k => $v)
	{
		$_GET[$k] = $v;
	}
}
#分类列表页面,未做
elseif(isset($_GET['modetag']) && isset($_GET['page']))
{

}
else
{
	//转至首页
	echo "404 page";exit;
}
require("index.php");