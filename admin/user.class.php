<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2017-11-01 23:43:23
#path : app/admin/user.class.php
*/
class user extends Admin
{
	
	public function indexcategoryAction()
	{
		$ajax = $this->security->get('ajax');
		$this->tpl->assign('ajax',$ajax);

		

		$db = m('group');
		$rs = $db->order("porder,id")
			->findAll();
		$this->tpl->assign('rs',$rs);


		
		$this->display();


		//p(load::$APP_RUN_SQLS);exit;
	}
	public function addcategoryAction()
	{
		$this->display();
	}
	public function doaddcategoryAction()
	{
		$input = array();
		$input['name'] = $this->security->get('name');
		$input['desc'] = $this->security->get('desc');
		$input['porder'] = $this->security->get('porder');
		$input['tag'] = $this->security->get('tag');

		$input['status'] = intval($this->security->get('status'));

		$db = m('group');
		$db->values($input)->add();
		$this->json(array('msg' => "{$input['name']}添加成功", 'status'=>1));

	}
	public function editcategoryAction()
	{
		$input['id'] = intval($this->security->get('id'));
		$db = m('group');
		$rs = $db->pk($input['id'])->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$input['id']}记录不存在", 'status'=>0));
		}
		$this->tpl->assign('rs',$rs);
		$this->display();
	}
	public function doeditcategoryAction()
	{
		$input['id'] = intval($this->security->get('id'));
		
		$input['name'] = $this->security->get('name');
		$input['desc'] = $this->security->get('desc');
		$input['porder'] = $this->security->get('porder');
		$input['tag'] = $this->security->get('tag');

		$input['status'] = intval($this->security->get('status'));

		$db = m('group');
		$rs = $db->pk($input['id'])->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$input['id']}记录不存在", 'status'=>0));
		}
		$db->values($input)->update();
		$this->json(array('msg' => "{$input['name']}编辑成功", 'status'=>1));
	}
	/**
	快速保存
	*/
	function fastcategoryAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->http['id'];
		
		
		$db = m('group');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));exit;
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));exit;
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		$input[$field] = $value;

		$db->values($input)->where("id='{$input['id']}'")->update();
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}
	function dodelcategoryAction()
	{
		$input['id'] = intval($this->security->get('id'));
		$db = m('group');
		if(!$input['id'])
		{
			$this->json(array('msg' => "ID不能为空,请重试", 'status'=>0));exit;
		}
		$db->pk($input['id'])->delete();

		$this->json(array('msg' => "删除成功", 'status'=>1));

	}
	/** 
		分组 绑定模块
	*/
	public function bindmoduleAction()
	{
		$id = intval($this->security->get('id'));
		$db = m('group');
		$db2 = m('module');
		$group = $db->pk($id)->find();
		$group['auth_arr'] = explode(',',substr($group['auth'],1,-1));


		$rs = $db2->getAdminOp();
		$this->tpl->assign('rs',$rs);

		$this->tpl->assign('group',$group);
		$this->tpl->assign('id',$id);


//		load::info();exit;

		

		

		$this->display();
		

	}

	public function dobindmoduleAction()
	{
		$id = $this->security->get('id');
		$opid = $this->security->get('op');
		$db = m('group');
		$rs = $db->pk($id)->find();
		if(!$rs)
		{
			$this->json(array('msg' => "记录不存在,请重试", 'status'=>0));exit;
		}

		$db2 = m('module');
		$rs = $db2->table('module_op')->where("status=1 AND is_admin=1")->findAll();
		$adminop = array();
		foreach($rs as $k => $v)
		{
			$adminop[$v['id']] = $v;
		}
		$auth = array();
		foreach($opid as $k => $v)
		{
			if(!isset($adminop[$v]))
			{
				continue;
			}			
			$auth[] = $adminop[$v]['module_id'] . '_' . $adminop[$v]['method'];
		}
		if(count($auth) >0)
		{
			$str = "," . implode(',', $auth) . ",";
		}
		else
		{
			$str = "";
		}
		$db->pk($id)->values(array("auth" => $str))->update();
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}

	
	public function indexAction()
	{
		
		$ajax = intval($this->security->get('ajax'));
		if($ajax)
		{
			//当前页数
			$page = intval($this->security->get('page'));
			//每页显示 $limit 条
			$limit = intval($this->security->get('limit'));
			$keyword = $this->security->get('keyword');
			//权限的意思: 自己不能编辑自己, 可以管理等级比自己低的用户
			$wstr = "a.id != '{$_SESSION['user']['id']}' AND 
			(a.level <= {$_SESSION['user']['level']}  )";
			if($keyword)
			{
				$wstr = "	a.login like '%{$keyword}%' OR 
				a.name like '%{$keyword}%' 
				";
			}
			$db = m("user");
			$total = $db->as('a')->where($wstr)->count();

			$r = $this->getLimit($page,$limit, $total);

			$rs = $db->order("porder,id")
				->limit($r['limit'])
				->as('a')
				->where($wstr)
				->leftJoin("group as b", "a.group_id=b.id")
				->field("a.*,b.name as group_name")
				->findAll();
			foreach($rs as $k => $v)
			{
				unset($rs[$k]['password']);
			}
			$this->tpl->assign('page',$r['limit']);
			$this->tpl->assign('keyword',$keyword);
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',$r['total']);
			//p($_GET);			P($keyword);



		}

		
		
		$this->tpl->assign('ajax',$ajax);
		$this->display();

	}
	/** */
	function addAction()
	{
		$db = m('group');
		$groups = $db->where("status=1")->getGroupId();

		$this->tpl->assign('groups',$groups);
		$db = m('user');
		$porder = $db->getMaxOrder();
		$this->tpl->assign('porder',$porder+1);
		
		$this->display();
	}
	/** */
	function doaddAction()
	{
		$input = array();

		$input['login'] = $this->security->http['login'];
		$input['name'] = $this->security->http['name'];
		$input['password'] = $this->security->http['password'];
		$input['group_id'] = $this->security->http['group_id'];
		$input['status'] = intval($this->security->get('status'));
		$input['remark'] = $this->security->http['remark'];
		$input['mobile'] = $this->security->http['mobile'];
		$input['qq'] = $this->security->http['qq'];
		$input['address'] = $this->security->http['address'];
		$input['porder'] = $this->security->http['porder'];

		$input['reg_date'] = time();
		$input['reg_ip'] = load::loadClass('http')->getIp();

		$db = m('user');
		if($db->checkUser($input['login']))
		{
			$this->json(array("msg" => "你好,用户名:{$input['login']}已经存在,请换一个", 'status' => 0));
		}
		if($input['password'])
		{
			$input['password'] = $this->secret->password($input['password']);
		}
		else
		{
			$this->json(array("msg" => "你好,密码不能为空", 'status' => 0));
		}

		$db->values($input)->add();
		$this->json(array('msg' => "{$input['login']}添加成功", 'status'=>1));

	}
	/** */
	function editAction()
	{
		$id = intval($this->security->get('id'));
		$db = m('user');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array("msg" => "你好,用户不存在", 'status' => 0));
		}
		unset($rt['password']);
		$this->tpl->assign('rt',$rt);

		$db = m('group');
		$groups = $db->where("status=1")->getGroupId();
		$this->tpl->assign('groups',$groups);


		$this->display();
	}
	/** */
	function doeditAction()
	{
		$id = intval($this->security->get('id'));
		$db = m('user');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array("msg" => "你好,用户不存在", 'status' => 0));
		}

		//$input['login'] = $this->security->http['login'];
		$input['name'] = $this->security->http['name'];
		$input['password'] = $this->security->http['password'];
		$input['group_id'] = $this->security->http['group_id'];
		$input['status'] = intval($this->security->get('status'));
		$input['remark'] = $this->security->http['remark'];
		$input['mobile'] = $this->security->http['mobile'];
		$input['qq'] = $this->security->http['qq'];
		$input['address'] = $this->security->http['address'];
		$input['porder'] = $this->security->http['porder'];

		if($input['password'])
		{
			$input['password'] = $this->secret->password($input['password']);
		}
		else
		{
			unset($input['password']);
		}
		$db->values($input)->pk($id)->update();
		$this->json(array('msg' => "用户编辑成功", 'status'=>1));

	}
	/** 删除操作*/
	function dodeleteAction()
	{
		$id = intval($this->security->get('id'));
		$db = m('user');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array("msg" => "你好,用户不存在", 'status' => 0));
		}
		$db->dodelete($id);
		$this->json(array('msg' => "删除用户:{$rt['login']}成功", 'status'=>1));
	}
	function fastAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->http['id'];
		
		
		$db = m('user');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		if($field == 'login')
		{
			$this->json(array('msg' => "{$field}字段不允许修改,请重试", 'status'=>0));
		}
		$input[$field] = $value;

		$db->values($input)->where("id='{$input['id']}'")->update();
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}
	public function myinfoAction()
	{
		
		$id = $_SESSION['user']['id'];
		$db = m('user');
		$rt = $db->pk($id)->find();
		unset(
			$rt['id'],
			$rt['reg_ip'],
			$rt['password'],$rt['porder'],$rt['remark'],$rt['level']);
		$this->tpl->assign('rt',$rt);
		
		$lang = parse_ini_file($this->tpl->getVar('SKIN')."cn.lang.php");

		$this->tpl->assign('lang',$lang );
		$this->display();
	}
	public function changepasswordAction()
	{
		$id = $_SESSION['user']['id'];
		$db = m('user');
		$rt = $db->pk($id)->find();
		unset($rt['password'],$rt['porder'],$rt['remark'],$rt['level']);
		$this->tpl->assign('rt',$rt);
		
		$lang = parse_ini_file($this->tpl->getVar('SKIN')."cn.lang.php");

		$this->tpl->assign('lang',$lang );

		

		$this->display();

	}
	public function dochangepasswordAction()
	{
		$id = $_SESSION['user']['id'];
		$db = m('user');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "修改个人信息时,用户记录不存在", 'status'=>0));
		}
		$input = array();
		$input['name'] = $this->security->post('name');
		$input['phone'] = $this->security->post('phone');
		$input['mobile'] = $this->security->post('mobile');
		$input['qq'] = $this->security->post('qq');
		$input['address'] = $this->security->post('address');
		$password = $this->security->post('password');
		$old_password = $this->security->post('old_password');
		if($password)
		{
			if(!$old_password)
			{
				$this->json(array('msg' => "您要修改密码,必须输入原来的正确密码", 'status'=>0));
			}
			else if($this->secret->password($old_password) != $rt['password'])
			{
				$this->json(array('msg' => "您要修改密码,必须输入原来的正确密码", 'status'=>0));
			}
			$input['password'] = $this->secret->password($password);
		}
		$db->pk($id)->values($input)->update();
		$this->json(array('msg' => "修改个人信息成功", 'status'=>1));



	}

}