<?php
/**<div class="layui-form-mid layui-word-aux">辅助文字</div>
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2018-03-02 13:03:33
#path : app/admin/news.class.php
*/
class news extends Admin
{
	public $html_tr = '';
	/** 
		Home
	*/
	public $must_field = ['id','category_id','title','name2','desc','keyword','content'];
	function indexAction()
	{
		$ajax = $this->security->get('ajax');
		$ajax = intval($ajax);
		$this->tpl->assign('ajax',$ajax);

		$mid = intval( $this->security->get('mid'));

		$cid = intval( $this->security->get('cid'));
		
		$db = m('mode');
		$modes = $db->getMode();
		$this->tpl->assign('modes', $modes);
		if(isset($modes[$mid]))
		{
			$mode = $modes[$mid];
		}
		else
		{			
			$mode = reset($modes);
			$mid = $mode['id'];	
		}
		$this->tpl->assign('WHERE', array($mode['name'] . "的列表"));

		$this->tpl->assign('mid',$mid);
		#获取此模型下的字段#渲染
		$fields = $db->where("status=1")->getModeFields($mid, false);
		$this->tpl->assign('fields',$fields);

		$db2 = m('category');
		$table = "my_" . $mode['tag'];
		$db2->getTotal($table);
		$tree = $db2->where("mid='{$mid}'")->getTreeObj();
		$html = $tree->select(array(
		'name' => 'cid',
		'default' => 0,
		'value' => $cid,
		'class' => 'layui-select',
		));
		$this->tpl->assign('fid_select',$html);
		unset($db2);


		if($ajax)
		{
			//当前页数
			$page = intval($this->security->get('page'));
			//每页显示 $limit 条
			$limit = intval($this->security->get('limit'));
			$keyword = $this->security->get('keyword');
			//权限的意思: 自己不能编辑自己, 可以管理等级比自己低的用户
			$wstr = "1 ";
			if($keyword)
			{
				$wstr .= " AND	a.title like '%{$keyword}%' ";
			}		

			if($cid)
			{
				$subids = $tree->getSubId($cid);
				$subids[] = $cid;
				$wstr .= " AND a.category_id IN (".implode(',',$subids).")";
			}
			$table = "my_" . $mode['tag'];

			$total = $db->table($table)->as('a')->where($wstr)->count();
			$r = $this->getLimit($page,$limit, $total);

			$rs = $db->order("id desc,title")
				->limit($r['limit'])
				->as('a')
				->table($table)
				->where($wstr)		
				->leftJoin('category as b','a.category_id=b.id')
				->field("a.*,b.name as category_name,b.id as category_id")				
				->findAll();

			$num = 1;
			foreach($rs as $k => $v)
			{
				$v['num'] = $num++;
				$rs[$k] = $v;
			}
			
			$this->tpl->assign('page',$r['limit']);
			$this->tpl->assign('keyword',$keyword);
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',$r['total']);
			
		}
		$this->display();
		
	}
	/** */
	function addAction()
	{
		$mid = intval($this->security->get('mid'));
		$this->tpl->assign('mid',$mid);
		$db = m('mode');
		$modes = $db->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));			
		}
		$mode = $modes[$mid];

		#获取此模型下的字段#渲染
		$fields = $db->getModeFields($mid);
		#渲染
		$this->tpl->assign('fields', $fields);
		$db = m('category');
		$table = "my_" . $mode['tag'];
		$db->getTotal($table);
		$tree = $db->where("mid='{$mid}'")->getTreeObj();

		$html = $tree->select(array(
		'name' => 'category_id',
		'default' => -1,
		'value' => 0,
		));
		$this->tpl->assign('category_select',$html);

		$this->display();
	}
	/** */
	function doaddAction()
	{
		$input = array(); 
		$input['category_id'] = intval($this->security->post('category_id')); 
		
		$mid = intval($this->security->post('mid')); 

		$db = m('mode');
		$modes = $db->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));			
		}
		$mode = $modes[$mid];

		#获取此模型下的字段#渲染
		$fields = $db->getModeFields($mid, false);

		foreach($fields as $k => $v)
		{
			$input[$v['field']] = $this->security->post($v['field']); 
		}
		if($this->security->get('_name2'))
		{
			$input['name2'] = load::loadClass('py')->pinyin($input['title']);
		}

		#操作TABLE
		$table = "my_"  . $mode['tag'];
		#验证一下自定义网址是否重复,
		if($input['name2'] && $db->table($table)->where("name2='{$input['name2']}'")->find())
		{
			$this->json(array('msg' => "自定义网址:{$input['name2']},已存在请重试", 'status'=>0));						
		}
		//p($input);		exit;
		$db->table($table)->values($input)->add();
		$this->json(array('msg' => "添加文章:{$input['title']} 成功", 'status'=>1));			
	}
	/** */
	function editAction()
	{
		$mid = intval($this->security->get('mid'));
		$id = intval($this->security->get('id'));
		$this->tpl->assign('mid',$mid);
		$this->tpl->assign('id',$id);
		$db = m('mode');
		$modes = $db->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));			
		}
		$mode = $modes[$mid];

		$table = "my_" . $mode['tag'];
		
		$rt = $db->table($table)->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请重试", 'status'=>0));			
		}


		#获取此模型下的字段#渲染
		$fields = $db->getModeFields($mid, true, $rt);
		#渲染
		$this->tpl->assign('fields', $fields);
		$db = m('category');
		$table = "my_" . $mode['tag'];
		$db->getTotal($table);
		$tree = $db->where("mid='{$mid}'")->getTreeObj();

		$html = $tree->select(array(
		'name' => 'category_id',
		'default' => -1,
		'value' => $rt['category_id'],
		));
		$this->tpl->assign('category_select',$html);



		$this->display();
	}
	/** */
	function doeditAction()
	{
		$input = array(); 
		$input['id'] = intval($this->security->post('id')); 
		$input['category_id'] = intval($this->security->post('category_id')); 
		$mid = intval($this->security->post('mid')); 

		$db = m('mode');
		$modes = $db->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));			
		}
		$mode = $modes[$mid];

		#获取此模型下的字段#渲染
		$fields = $db->getModeFields($mid, false);

		foreach($fields as $k => $v)
		{
			$input[$v['field']] = $this->security->post($v['field']); 
		}
		if($this->security->get('_name2'))
		{
			$input['name2'] = load::loadClass('py')->pinyin($input['title']);
		}
		
		
		#操作TABLE
		$table = "my_"  . $mode['tag'];

		#验证一下自定义网址是否重复,
		if($input['name2'] && $db->where("id!='{$input['id']}'")->table($table)->where("name2='{$input['name2']}'")->find())
		{
			$this->json(array('msg' => "自定义网址:{$input['name2']},已存在请重试", 'status'=>0));						
		}

		//p($input);		exit;
		$db->table($table)->values($input)->pk($input['id'])->update();
		$this->json(array('msg' => "编辑文章:{$input['title']} 成功", 'status'=>1));	

	}
	/** 文章处理function deleteAction()
	{
		$input = array(); 
		$input['id'] = intval($this->security->post('id')); 
		$mid = intval($this->security->post('mid')); 

		$db = m('mode');
		$modes = $db->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));			
		}
		$mode = $modes[$mid];
		#操作TABLE
		$table = "my_"  . $mode['tag'];
		$rt = $db->table($table)->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "文章记录不存在,请换一个", 'status'=>0));
		}
		$db->table($table)->pk($id)->delete();
		$this->json(array('msg' => "删除文章:{$rt['title']} 成功", 'status'=>1));		
	}*/
	
	/** */
	public function indexmodeAction()
	{

		//$this->help->readme('news');exit;
		$ajax = $this->security->get('ajax');
		$ajax = intval($ajax);
		$this->tpl->assign('ajax',$ajax);
		
		if($ajax)
		{
			//当前页数
			$page = intval($this->security->get('page'));
			//每页显示 $limit 条
			$limit = intval($this->security->get('limit'));
			$keyword = $this->security->get('keyword');
			//权限的意思: 自己不能编辑自己, 可以管理等级比自己低的用户
			$wstr = "";
			if($keyword)
			{
				$wstr = "	a.tag like '%{$keyword}%' OR 
				a.name like '%{$keyword}%' 
				";
			}
			$db = m("mode");
			$total = $db->as('a')->where($wstr)->count();

			$r = $this->getLimit($page,$limit, $total);

			$rs = $db->order("porder,id")
				->limit($r['limit'])
				->as('a')
				->where($wstr)				
				->field("a.*")
				->findAll();

			$a = $db->table('mode_field')->field("count(*) as total,mid")->group('mid')->findAll();
			foreach($a as $k => $v)
			{
				$rec[$v['mid']] = $v['total'];
			}
			$num = 1;
			foreach($rs as $k => $v)
			{
				if(isset($rec[$v['id']]))
				{
					$v['total'] = $rec[$v['id']];
				}
				else
				{
					$v['total'] = 0;
				}
				$v['num'] = $num++;
				$rs[$k] = $v;
			}
			
			
			$this->tpl->assign('page',$r['limit']);
			$this->tpl->assign('keyword',$keyword);
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',$r['total']);
		}		

		$this->display();
	}
	/**
	快速编辑列表字段,文章
	*/
	function dofastAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = intval($this->security->post('id'));
		$mid = intval($this->security->get('mid'));
		$db = m('mode');
		$modes = $db->where("id='{$mid}'")->getMode();
		if(!isset($modes[$mid]))
		{
			$this->json(array('msg' => "模型不存在,请重试", 'status'=>0));
		}
		$mode = $modes[$mid];		
		$table = "my_" . $mode['tag'];
		
		
		$tmp = $db->where("status=1")->getModeFields($mid, false);
		foreach($tmp as $k => $v)
		{
			$fields[$v['field']] = $v;
		}
		if(!isset($fields[$field]))
		{
			$this->json(array('msg' => "字段不存,只允许修改列表,请重试", 'status'=>0));
		}
		
		$rt = $db->table($table)->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		
		$input[$field] = $value;
		#验证一下自定义网址是否重复,
		if($field == 'name2' && $value && $db->where("id!='{$input['id']}'")->table($table)->where("name2='{$value}'")->find())
		{
			$this->json(array('msg' => "自定义网址:{$value},已存在请重试", 'status'=>0));						
		}

		$db->values($input)->table($table)->where("id='{$input['id']}'")->update();
		
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}
	/**
	添加模型
	*/
	public function addmodeAction()
	{
		$this->display();
	}
	/**
	处理添加模型
	*/
	public function doaddmodeAction()
	{
		$input = array(); 
		$input['tag'] = $this->security->post('tag'); 
		$input['name'] = $this->security->post('name'); 
		$input['desc'] = $this->security->post('desc'); 
		$input['status'] = $this->security->post('status'); 
		$input['porder'] = $this->security->post('porder'); 

		$input['tpl_index'] = $this->security->post('tpl_index'); 
		$input['tpl_category'] = $this->security->post('tpl_category'); 
		$input['tpl_content'] = $this->security->post('tpl_index'); 

		$input['status'] = intval($input['status']);
		$input['porder'] = intval($input['porder']);
		if(!$input['tag'])
		{
			$this->json(array('msg' => "标记不能为空", 'status'=>0));
		}
		$db = m('mode');

		
		
		if($db->check($input['tag']))
		{
			$this->json(array('msg' => "标记:{$input['tag']}已经存在,请换一个", 'status'=>0));
		}
		$input['id'] = $db->values($input)->add();

		#自动添加常用的字段,建表
		$db->addMode($input);


		$this->json(array('msg' => "添加模型 {$input['tag']} :{$input['name']}成功", 'status'=>1));		
	}
	/***/
	public function editmodeAction()
	{
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('mode');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		$rt['desc'] = html_entity_decode($rt['desc']);
		$this->tpl->assign('rt', $rt);
		$this->display();
	}
	/***/
	public function doeditmodeAction()
	{
		$input = array(); 
		$input['tag'] = $this->security->post('tag'); 
		$input['name'] = $this->security->post('name'); 
		$input['desc'] = $this->security->post('desc'); 
		$input['status'] = $this->security->post('status'); 
		$input['porder'] = $this->security->post('porder'); 

		$input['tpl_index'] = $this->security->post('tpl_index'); 
		$input['tpl_category'] = $this->security->post('tpl_category'); 
		$input['tpl_content'] = $this->security->post('tpl_index');

		$input['id'] = $this->security->post('id'); 

		$input['id'] = intval($input['id']);
		$input['status'] = intval($input['status']);

		$db = m('mode');
		$rt = $db->pk($input['id'])->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		#标记不同,验证一下
		if($rt['tag'] != $input['tag'])
		{			
			if($db->where("id!='{$input['id']}'")->check($input['tag']))
			{
				$this->json(array('msg' => "标记:{$input['tag']}已经存在,请换一个", 'status'=>0));
			}
		}
		$db->values($input)->update();
		$db->updateTable($input, $rt);
		
		$this->json(array('msg' => "编辑模型:{$rt['name']}成功", 'status'=>1));		

	}
	public function dodeleteAction()
	{
		$mid = intval($this->security->get('mid'));
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('mode');
		$mode = $db->pk($mid)->find();
		if(!$mode)
		{
			$this->json(array('msg' => "模型记录不存在,请换一个", 'status'=>0));
		}
		$table = "my_" . $mode['tag'];

		$rt = $db->table($table)->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "文章记录不存在,请换一个", 'status'=>0));
		}
		$db->table($table)->pk($id)->delete();
		$this->json(array('msg' => "删除文章:{$rt['title']} 成功", 'status'=>1));		

	}
	/**
	快速保存
	*/
	function dofastmodeAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->post('id');
		$input['id'] = intval($input['id']);
		$db = m('mode');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);

		

	
		
		if(!isset($rt[$field]) )
		{
			//$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		if($field == 'tag')
		{
			#标记不同,验证一下
			$input['tag'] = $value;
			if($rt['tag'] != $input['tag'])
			{
				if($db->where("id!='{$input['id']}'")->check($input['tag']))
				{
					$this->json(array('msg' => "标记:{$input['tag']}已经存在,请换一个", 'status'=>0));
				}
			}
			//$this->json(array('msg' => "{$field}字段不允许修改,请重试", 'status'=>0));
		}
		$input[$field] = $value;

		$db->values($input)->where("id='{$input['id']}'")->update();
		#更新模型表
		$db->updateTable($input['id']);

		$this->json(array('msg' => "快速编辑模型成功", 'status'=>1));
	}
	/**
	功能说明:	
	创建日期:2018-03-04
	*/
	public function indexfieldAction()
	{


		$mid = intval($this->security->get('mid'));
		$db = m('mode');
		$rt = $db->pk($mid)->find();
		if(!$rt)
		{
			$this->json(array('msg' => $db->lastsql()."模块记录不存在,请重试", 'status'=>0));
		}
		$ajax = $this->security->get('ajax');
		$this->tpl->assign('ajax',$ajax);
		$this->tpl->assign('mid',$mid);
		$this->tpl->assign('mode_rt',$rt);
		$this->tpl->assign('WHERE',array("{$rt['name']}({$rt['tag']})"));

		if($ajax)
		{
			$db = m("mode");
			$wstr = "mid='{$mid}'";
			$rs = $db->order("porder,id")
				->as('a')
				->table('mode_field')
				->where($wstr)				
				->field("a.*")
				->findAll();
			$num = 1;
			foreach($rs as $k => $v)
			{
				$rs[$k]['form'] = htmlspecialchars($v['form']);
				$rs[$k]['num'] = $num++;
			}
			
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',count($rs));
		}

		

		$this->display();		
	}
	/**
	功能说明:	
	创建日期:2018-03-04
	*/
	public function addfieldAction()
	{
		$mid = intval($this->security->get('mid'));
		$db = m('mode');
		$rt = $db->pk($mid)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "模块记录不存在,请重试", 'status'=>0));
		}
		$this->tpl->assign('mid',$mid);

		$this->display();		
	}
	/**
	功能说明:	
	创建日期:2018-03-04
	*/
	public function doaddfieldAction()
	{
		$input = array(); 
		$input['mid'] = $this->security->post('mid'); 
		$input['field'] = $this->security->post('field'); 
		$input['name'] = $this->security->post('name'); 
		$input['form'] = $this->security->post('form'); 
		$input['request'] = $this->security->post('request'); 
		$input['porder'] = $this->security->post('porder');
		$input['status'] = intval($this->security->post('status'));

		$input['request'] = intval($input['request']);
		$input['form'] = str_replace("\n", '', nl2br($input['form']));

		$db = m("mode");
		$mode_rt = $db->pk($input['mid'])->find();
		if(!$mode_rt)
		{
			$this->json(array('msg' => "模块记录不存在,请重试", 'status'=>0));
		}
		if($input['field'] == 'id')
		{
			$this->json(array('msg' => "ID为保留字段,请换一个", 'status'=>0));
		}
		if($db->checkField($input['field'], $input['mid']))
		{
			$this->json(array('msg' => "字段:{$input['field']}已经存在,请换一个", 'status'=>0));
		}
		#选择一下 内容表中的MAP值
		//$input['map'] = $db->getNewMap($input['mid']);

		$db->table("mode_field")->values($input)->add();

		$input['tag'] = $mode_rt['tag'];
		$db->addField($input);

		$this->json(array('msg' => "添加字段:{$input['name']}成功", 'status'=>1));		
		


	}
	/**
	功能说明:	
	创建日期:2018-03-04
	*/
	public function editfieldAction()
	{
		$mid = intval($this->security->get('mid'));
		$db = m('mode');
		$rt = $db->pk($mid)->find();
		
		if(!$rt)
		{
			$this->json(array('msg' => "模块记录不存在,请重试", 'status'=>0));
		}
		$this->tpl->assign('mid',$mid);

		$id = intval($this->security->get('id'));
		$rt = $db->table("mode_field")
			->where("id='{$id}'")
			->find();
		if(!$rt)
		{
			
			$this->json(array('msg' => "字段记录不存在,请重试", 'status'=>0));
		}

		
		$rt['form'] = addslashes(html_entity_decode($rt['form']));

		$this->tpl->assign('rt', $rt);
		$this->display();



	}	

	/**
	功能说明:	
	创建日期:2018-03-04
	*/
	public function doeditfieldAction()
	{
		$input = array(); 
		$input['id'] = $this->security->post('id'); 
		$input['mid'] = $this->security->post('mid'); 
		$input['field'] = $this->security->post('field'); 
		$input['name'] = $this->security->post('name'); 
		$input['form'] = $this->security->post('form'); 
		$input['request'] = $this->security->post('request'); 
		$input['porder'] = $this->security->post('porder'); 

		$input['id'] = intval($input['id']);
		$input['request'] = intval($input['request']);
		$input['porder'] = intval($input['porder']);
		$input['status'] = intval($this->security->post('status'));

		$input['form'] = str_replace("\n", '', nl2br($input['form']));
		

		$db = m("mode");
		$mode_rt = $db->pk($input['mid'])->find();
		if(!$mode_rt)
		{
			$this->json(array('msg' => "模块记录不存在,请重试", 'status'=>0));
		}
		$rt = $db->table2()->pk($input['id'])->find();
		if(!$rt)
		{
			$this->json(array('msg' => "字段记录不存在,请重试", 'status'=>0));
		}
		if($input['field'] != $rt['field'] && in_array($rt['field'], $this->must_field))
		{
			$this->json(array('msg' => "字段:{$input['field']}为固定字段,不允许修改", 'status'=>0));
		}
		if($db->where("id!='{$input['id']}'")->checkField($input['field'], $input['mid']))
		{
			$this->json(array('msg' => "字段:{$input['field']}已经存在,请换一个", 'status'=>0));
		}
		#MAP值,不允许修改
		//unset($input['map']);

		
		$db->updateField($input, $input['id']);

		
		$this->json(array('msg' => "编辑字段:{$input['name']}成功", 'status'=>1));	

	}
	public function dofastfieldAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->post('id');
		$input['id'] = intval($input['id']);
		$db = m('mode');
		
		$rt = $db->table("mode_field")
			->as('a')
			->leftJoin("mode as b", "a.mid=b.id")
			->field("a.*,b.tag")
			->where("a.id='{$input['id']}'")->find();	

		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		if(!isset($rt[$field]) || $field == 'id')
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		if($field == 'field')
		{
			#标记不同,验证一下
			$input['field'] = $value;
			if($rt['field'] != $input['field'])
			{				
				if(in_array($rt['field'], $this->must_field))
				{
					$this->json(array('msg' => "字段:{$input['field']}为固定字段,不允许修改", 'status'=>0));
				}

				if($db->where("id!='{$input['id']}'")->checkField($input['field'], $rt['mid']))
				{
					$this->json(array('msg' => "字段:{$input['field']}已经存在,请换一个", 'status'=>0));
				}
			}
			//$this->json(array('msg' => "{$field}字段不允许修改,请重试", 'status'=>0));
		}
		$new = $input;
		$new[$field] = $value;
		$db->updateField($new, $rt);

		/*
		$db->values($input)
			->table("mode_field")
			->where("id='{$input['id']}'")->update();
		*/

		$this->json(array('msg' => "快速编辑字段成功", 'status'=>1));

	
	}
	public function dodeletefieldAction()
	{
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('mode');
		$rt = $db->table("mode_field")
			->as('a')
			->leftJoin("mode as b", "a.mid=b.id")
			->field("a.*,b.tag")
			->where("a.id='{$id}'")->find();

		
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		$db->dodelfield($rt);
		$this->json(array('msg' => "删除字段:{$rt['name']}({$rt['field']})成功", 'status'=>1));		


	}

	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public function addcategoryAction()
	{
		$mid = intval($this->security->get('mid'));
		$fid = intval($this->security->get('fid'));
		$this->tpl->assign('mid',$mid);
		$this->tpl->assign('fid',$fid);

		$db = m('category');
		$tree = $db->where("mid='{$mid}'")->getTreeObj();

		$html = $tree->select(array(
		'name' => 'fid',
		'default' => 0,
		'value' => $fid,
		));
		
		$this->tpl->assign('fid_select',$html);

		$this->display();
	}	
	
	function doaddcategoryAction()
	{
		#fid 上一级分类ID , mid 表示模型ID
		$input = array(); 
		$input['fid'] = $this->security->post('fid'); 
		$input['name'] = $this->security->post('name'); 
		$input['name2'] = $this->security->post('name2');
		$input['keyword'] = $this->security->post('keyword'); 
		$input['desc'] = $this->security->post('desc'); 
		$input['show'] = $this->security->post('show'); 
		$input['porder'] = $this->security->post('porder'); 
		$input['mid'] = $this->security->post('mid'); 
		$py = $this->security->get('py'); 
		if($py)
		{
			//$input['name2'] = $this->py()->pinyin($input['name']);
			$input['name2'] = load::loadClass('py')->pinyin($input['name']);
		}

		$input['mid'] = intval($input['mid']);
		$input['show'] = intval($input['show']);
		$input['porder'] = intval($input['porder']);
		$input['fid'] = intval($input['fid']);
		
		$db = m('category');
		if($db->where("mid='{$input['mid']}'")->check($input['name2']))
		{
			$this->json(array('msg' => "别名:{$input['name2']}已经存在,请换一个", 'status'=>0));
		}
		$db->values($input)->add();
		$this->json(array('msg' => "添加分类成功 {$input['name']} :{$input['name2']}成功", 'status'=>1));		
	}
	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public function editcategoryAction()
	{		
		$id = intval($this->security->get('id'));
//		$this->tpl->assign('fid',$fid);

		$db = m('category');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请重试", 'status'=>0));
		}
		$fid = $rt['fid'];
		$mid = $rt['mid'];


		$tree = $db->where("mid='{$mid}'")->getTreeObj();

		$html = $tree->select(array(
		'name' => 'fid',
		'default' => 0,
		'value' => $fid,
		));
		$this->tpl->assign('fid_select',$html);
		$this->tpl->assign('rt',$rt);

		$this->display();


	}	
	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public function doeditcategoryAction()
	{	
		#fid 上一级分类ID , mid 表示模型ID
		$input = array(); 
		$input['id'] = $this->security->post('id'); 
		$input['fid'] = $this->security->post('fid'); 
		$input['name'] = $this->security->post('name'); 
		$input['name2'] = $this->security->post('name2');
		$input['keyword'] = $this->security->post('keyword'); 
		$input['desc'] = $this->security->post('desc'); 
		$input['show'] = $this->security->post('show'); 
		$input['porder'] = $this->security->post('porder'); 
		$py = $this->security->get('py'); 
		if($py)
		{
			$input['name2'] = load::loadClass('py')->pinyin($input['name']);
		}
		$input['show'] = intval($input['show']);
		$input['porder'] = intval($input['porder']);
		$input['fid'] = intval($input['fid']);
		$input['id'] = intval($input['id']);
		
		$db = m('category');
		$rt = $db->pk($input['id'])->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		$input['mid'] = $rt['mid'];
		if($input['fid'] && $input['fid'] == $input['id'])
		{
			$this->json(array('msg' => "不能将自己设成上级分类", 'status'=>0));
		}


		if($input['name2'] && $db->where("mid='{$input['mid']}' AND id!='{$input['id']}'")->check($input['name2']))
		{
			$this->json(array('msg' => "别名:{$input['name2']}已经存在,请换一个", 'status'=>0));
		}
		$db->values($input)->pk($input['id'])->update();
		$this->json(array('msg' => "编辑分类成功 {$input['name']} :{$input['name2']}成功", 'status'=>1));		

	}


	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public function dodeletecategoryAction()
	{
		$id = intval($this->security->get('id'));
		$db = m('category');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}

		$db->dodelcategory($id);
		$this->json(array('msg' => "删除模型:{$rt['name']}({$rt['name2']})成功", 'status'=>1));	

	}

	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public function dofastcategoryAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->post('id');
		$input['id'] = intval($input['id']);
		

		$db = m('category');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		$input[$field] = trim($value);
		
		if($field == 'name2')
		{
			#标记不同,验证一下
			$input['name2'] = $this->input->safeString($value);
			if($rt['name2'] != $input['name2'])
			{
				if($input['name2'] && $db->where("mid='{$rt['mid']}' AND id!='{$input['id']}'")->check($input['name2']))
				{
					$this->json(array('msg' => "别名:{$input['name2']}已经存在,请换一个", 'status'=>0));
				}
			}
			//$this->json(array('msg' => "{$field}字段不允许修改,请重试", 'status'=>0));
		}
		if($field == 'name')
		{
			//为了去掉 1. 格式,
			$input[$field] = explode('.',$input[$field]);
			$input[$field] = end($input[$field]);
		}
		
		
		


		$db->values($input)->where("id='{$input['id']}'")->update();
		#更新模型表
		$db->updateTable($input['id']);

		$this->json(array('msg' => "保存成功", 'status'=>1));

	}
	/**
	功能说明:	
	创建日期:2018-03-05
	*/
	public $total = array();
	public function categoryAction()
	{

		$ajax = $this->security->get('ajax');
		$ajax = intval($ajax);
		$this->tpl->assign('ajax',$ajax);

		$mid = intval($this->security->get('mid'));
		
		$db = m('mode');
		$modes = $db->getMode();
		$this->tpl->assign('modes', $modes);		
		if(isset($modes[$mid]))
		{
			$mode = $modes[$mid];			
		}
		else
		{			
			$mode = reset($modes);
			$mid = $mode['id'];	
		}
		$this->tpl->assign('WHERE', array($mode['name'] . "的分类"));
		$this->tpl->assign('mid',$mid);
		
		//$this->help->auto('load');exit;
		$db = m('category');
		$tree = $db->where("mid='{$mid}'")->getTreeObj();

		$rs = $tree->get(0);
		if(!is_array($rs))
		{
			$rs = array();
		}
		/*计算各分类的文章数*/
		$table = "my_" . $mode['tag'];
		
		$this->total = $db->getTotal($table);
		

		$this->tpl->assign('rs',$rs);

		$this->html_tr($rs);
		$this->tpl->assign('html_tr',$this->html_tr);


		$this->display();
	}
	
	#递归调用
	function html_tr($rs,$sep = '')
	{
		$cnt = 1;
		$sep .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		foreach($rs as $k => $v)
		{
			if($v['fid'] == 0)
			{
				$sep = '';
			}			
			$v['keyword'] && $v['keyword'] = "{$v['keyword']}";
			$v['show'] ? $v['show'] = "<span class='label label-primary'>正常</span>" : $v['show'] = "<span class='label label-danger'>隐藏</span>";

			$total = isset($this->total[$v['id']]) ? $this->total[$v['id']] : 0;
			$this->html_tr .= "<tr >
			<td>{$v['id']}</td>
        <td>{$sep}{$cnt}.{$v['name']}</td>
        <td>{$sep}{$v['name2']}</td>
        <td>{$v['porder']}</td>

		<td>{$v['show']}</td>
		<td>{$total}</td>
		<td>
			<a class=\"layui-btn layui-btn-mini add icon_add\" lay-event=\"add\">添加子分类</a>

		<a class=\"layui-btn layui-btn-mini edit icon_edit\" lay-event=\"edit\" >编辑分类</a>
		<a class=\"layui-btn layui-btn-danger layui-btn-mini del icon_del\" lay-event=\"del\">删除分类</a> 
	

		</td>
     </tr>";
			$cnt++;
			if(isset($v['child']))
			{
				$this->html_tr($v['child'],$sep);
			}
		}
	}
}