<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2017-10-25 11:09:58
#path : app/admin/index.class.php
*/
class index extends Admin
{	

	
	function indexAction()
	{
		#设置,保存 标签菜单
		$tag = $this->security->get('tag');
		if($tag)
		{
			$_SESSION['user']['tag'] = $tag;
			//$this->router->jump($this->router->url("index",'index'));
		}
		#设置,保存 标签菜单 __END
		$this->display();
	}
	public function loginAction()
	{		
		//可以换多个登陆模板 
		//$this->display();exit;
		$this->tpl->assign('user', $this->security->get('user'));
		$this->tpl->assign('password', $this->security->get('password'));
		$this->display("index.movelogin.html");
	}
	public function logoutAction()
	{
		session_destroy();
		$this->router->jump($this->router->url('index'));

	}	
	public function dologinAction()
	{				
		$user = $this->security->post('user');
		$password = $this->security->post('password');
		
		$db = m('user');
		$rs = $db->where("login='{$user}'")->find();
		if (!$rs)
		{
			$this->json(array('msg' => "用户名不存在", 'status'=>0));
			
		}
		if ($rs['password'] != $this->secret->password($password))
		{
			$this->json(array('msg' => "密码错误", 'status'=>0));
		}

		
		
		if (!$rs['status'])
		{
			//$this->writeLog("用户:{$user}(禁用),试图登陆系统,被禁止");
			$this->json(array('msg' => "您已经被系统禁用", 'status'=>0));
		}

		#取得用户权限
		if ($rs['level'] <= 2)
		{			
			$id = $rs['id'];
			$db2 = m('group');
			$group = $db2->pk($rs['group_id'])->find();
			
			$rs['auth'] = explode(',',substr($group['auth'],1,-1));

			//公共可用
			$rs['auth'][] = 'index_login';
			$rs['auth'][] = 'index_logout';
			$rs['auth'][] = 'index_index';
			//$rs['auth'] = array_filp($rs['auth']);
		}
		else 
		{
			$rs['auth'] = '*';
		}
		unset($rs['password']);
		
		//exit;
		#登陆成功
		$rs['update'] = time();
		
		$_SESSION['user'] = $rs;
		$_SESSION['user']['tag'] = 'all';

		//$this->writeLog("用户:{$rs['login']}登陆成功");

			
		$this->json(array('msg' => "用户:{$rs['login']}登陆成功", 'status'=>1,'url' => $this->router->url('index')));

	
	}



}