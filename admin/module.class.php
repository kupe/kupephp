<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2017-11-01 23:24:47
#path : app/admin/module.class.php
*/
class module extends Admin
{
	
	function __construct()
	{
		parent::__construct();
		if($this->router->m != 'index')
		{
			#刷新缓存
			$this->cacheing->flush();
		}
	}
	/** 
		Home
	*/
	function indexAction()
	{
		$db = m('module');


		$toggle = $this->security->get('toggle');
		if($toggle)
		{
			//保存菜单展示
			$data = $this->security->get('data');
			list($module,$display) = explode("|",$data);
			$display = $display == 'none' ? 0 : 1;
			$db->set("show_sub='{$display}'")->where("module='{$module}'")->update();
			exit;
		}		
		$rs = $db->order('porder asc,add_time asc')->findAll();
		$this->tpl->assign('rs',$rs);

		$ops = $db->getOpAll();
		
		$this->tpl->assign('ops', $ops );

		$ajax = $this->security->get('ajax');

		$this->tpl->assign('ajax', $ajax);
		

		$this->display();
	}
	/** */
	function addAction()
	{
		$db = m('module');
		$this->tpl->assign('porder', $db->getMaxOrder()+1);;

		

		$this->display();
	}
	/** 
	执行添加操作
	*/
	function doaddAction()
	{
		$input = array();
		$input['name'] = $this->security->http['name'];
		$input['module'] = $this->security->http['module'];
		$input['status'] = intval($this->security->get('status'));
		$input['desc'] = $this->security->http['desc'];
		$input['auth'] = $this->security->http['auth'];
		$input['porder'] = $this->security->http['porder'];
		$input['tag'] = $this->security->get('tag');

		$input['add_time'] = time();

		$db = m('module');
		$rt = $db->where("module='{$input['module']}'")->find();
		if($rt)
		{
			$this->json(array('msg' => "{$input['module']}已经存在,请换一个", 'status'=>0));exit;
		}
		
		$db->values($input)->add();
		


		$this->json(array('msg' => "{$input['name']}添加成功", 'status'=>1));
		//$this->json(array('msg' => "{$input['id']}添加失败", 'status'=>0));exit;
		
	}
	/** */
	function editAction()
	{
		$module = $this->security->get('module');
		$db = m('module');
		$rt = $db->where("module='{$module}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$module}不存在,请重试", 'status'=>0));exit;
		}
		$this->tpl->assign('rt',$rt);

		$this->display();
	}
	/** */
	function doeditAction()
	{
		$input = array();
		$input['name'] = $this->security->http['name'];
		$module = $this->security->http['module'];
		$input['status'] = intval($this->security->get('status'));
		$input['desc'] = $this->security->http['desc'];
		$input['auth'] = $this->security->http['auth'];
		$input['porder'] = $this->security->http['porder'];
		$input['tag'] = $this->security->get('tag');
		//$input['add_time'] = time();

		$db = m('module');
		$rt = $db->where("module='{$module}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$module}不存在,请重试", 'status'=>0));exit;
		}


		
		$db->values($input)
			->where("module='{$module}'")
			->update();
		
		$this->json(array('msg' => "{$input['name']} 编辑成功", 'status'=>1));


	}
	/** */
	function dodelAction()
	{
		$module = $this->security->http['module'];
		if(!$module)
		{
			$this->json(array('msg' => "ID不能为空,请重试", 'status'=>0));exit;
		}
		$db = m('module');
		$db->del($module);

		$this->json(array('msg' => "删除成功", 'status'=>1));
		
	}
	
	/**
	快速保存
	*/
	function fastAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['module'] = $this->security->http['id'];
		
		$input[$field] = $value;
		$db = m('module');
		
		$rt = $db->where("module='{$input['module']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));exit;
		}
		unset($rt['module']);
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段不存在,请重试", 'status'=>0));exit;
		}
		if($field == 'status')
		{
			$value = intval($value);
		}
		else
		{
			$value = $this->security->safeString($value);
		}
		$db->values($input)->where("module='{$input['module']}'")->update();
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}

	/***/
	public function addopAction()
	{
		//id = module_id
		$module = $this->security->get('module');
		$db = m('module');
		$this->tpl->assign('porder', $db->where("module_id='{$module}'")->getMaxOrderOp()+1);
		$rs = $db->where("module='{$module}'")->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$module}不存在,请重试", 'status'=>0));exit;
		}
		$this->tpl->assign('rs',$rs);

		$ops = $db->where("module_id='{$module}'")->getOpAll();
		if(isset($ops[$module]))
		{
			$op = $ops[$module];
		}
		else
		{
			$op = array();
		}
		
		$this->tpl->assign('op', $op);

		$this->display();
	}
	/***/
	public function doaddopAction()
	{
		$module = $this->security->get('module');
		$db = m('module');
		$rs = $db->where("module='{$module}'")->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$module}不存在,请重试", 'status'=>0));exit;
		}
		$input = array();
		$input['module_id'] = $module;
		$input['name'] = $this->security->http['name'];		
		$input['method'] = $this->security->http['method'];		
		$input['status'] = intval($this->security->get('status'));
		$input['desc'] = $this->security->http['desc'];
		$input['other'] = $this->security->http['other'];
		$input['tpl'] = $this->security->http['tpl'];
		$input['porder'] = floatval($this->security->http['porder']);
		$input['is_admin'] = intval($this->security->http['is_admin']);
		$input['is_menu'] = intval($this->security->get('is_menu'));

		if($db->checkOpExists($input))
		{
			$this->json(array('msg' => "{$input['module_id']} => {$input['method']} 操作已经存在,请重试", 'status'=>0));
		}

		/*自动添加一个用于后台的模块*/
		if($input['method']{0} == '#')
		{
			$input['method'] = substr($input['method'],1);
			$input2 = $input;
			$input2['method'] = 'do' .$input2['method'] ;
			$input2['name'] = '后台执行->' .$input2['name'] ;
			$input2['is_menu'] = 0;
			$input2['porder']++;
		}
		else
		{
			$input2 = false;
		}

		$db->table('module_op')
			->values($input)
			->add();	
		if($input2)
		{
			$db->table('module_op')->values($input2)->add();
		}
		$this->json(array('msg' => "添加[{$input['name']}]成功", 'status'=>1));

	}
	public function viewAction()
	{
		$id = $input['id'] = $this->security->http['id'];
		$ajax = $this->security->get('ajax');

		$db = m('module');
		$rs = $db->where("id='{$id}'")->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$id}不存在,请重试", 'status'=>0));exit;
		}
		$ops = $db->where("id='{$id}'")->getOpAll();
		
		if(isset($ops[$id]))
		{
			$op = $ops[$id];
		}
		else
		{
			$op = array();
		}
		$this->tpl->assign('op', $op);
		$this->tpl->assign('id', $id);
		$this->tpl->assign('ajax', $ajax);

		$this->display();
	}

	/**
	快速保存
	*/
	function fastopAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->http['id'];
		
		$input[$field] = $value;
		$db = m('module');
		
		$rt = $db->table('module_op')->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => '=='.$input['id'] . "数据不存在,请重试", 'status'=>0));exit;
		}
		unset($input['id']);	//不允许编辑模块ID		
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段不存在,请重试", 'status'=>0));exit;
		}
		if($field == 'status' || $field == 'is_menu')
		{
			$value = intval($value);
		}
		if($field == 'porder')
		{
			$value = floatval($value);
		}


		$db->table('module_op')
			->where("`id`='{$rt['id']}'")
			->values($input)->update();
		$this->json(array('msg' => "保存成功", 'status'=>1));
	}
	/***/
	public function editopAction()
	{
		
		//unique_id = id
		$id = $this->security->get('id');
		//list($id, $method) = explode('_',$unique_id);

		$db = m('module');
		$rs = $db->table("module_op")->where("id='{$id}'")->find();

		if(!$rs)
		{
			$this->json(array('msg' => "{$id}不存在,请重试", 'status'=>0));exit;
		}
		
		$this->tpl->assign('rs',$rs);

		$ops = $db->where("module_id='{$rs['module_id']}'")->getOpAll();
		if(isset($ops[$rs['module_id']]))
		{
			$op = $ops[$rs['module_id']];
		}
		else
		{
			$op = array();
		}
		
		$this->tpl->assign('op', $op);

		$this->display();
	}
	/***/
	public function doeditopAction()
	{
		$input = array();
		$id = intval($this->security->http['id']);

		$db = m('module');
		$rs = $db->table("module_op")->where("id='{$id}'")->find();
		if(!$rs)
		{
			$this->json(array('msg' => "{$id}不存在,请重试", 'status'=>0));exit;
		}
	
		$input['name'] = $this->security->http['name'];		
		$input['method'] = $this->security->http['method'];		
		$input['status'] = intval($this->security->get('status'));
		$input['desc'] = $this->security->http['desc'];
		$input['other'] = $this->security->http['other'];
		$input['tpl'] = $this->security->http['tpl'];
		$input['porder'] = floatval($this->security->http['porder']);
		$input['is_admin'] = intval($this->security->get('is_admin'));
		$input['is_menu'] = intval($this->security->get('is_menu'));
		
		


		/*
		if(!$db->checkOpExists($input))
		{
			$this->json(array('msg' => "{$input['unique_id']}--{$input['id']} => {$input['method']} 不存在,请重试", 'status'=>0));
		}
		*/

		

		$db->table('module_op')
			->values($input)
			->where("id='{$id}'")
			->update();	
		$this->json(array('msg' => "编辑[{$input['name']}]成功", 'status'=>1));

	}
	/** */
	function dodeleteopAction()
	{
		$id = intval($this->security->get('id'));
		if(!$id)
		{
			$this->json(array('msg' => "ID不能为空,请重试", 'status'=>0));exit;
		}
		$db = m('module');
		$db->delop($id);

		$this->json(array('msg' => "删除成功", 'status'=>1));
		
	}
	
	/**
	自动导入某个 类下的操作
	$id 
	*/
	public function autoimplodeAction()
	{
		$module = $this->security->get('id');
		$db = m('module');
		$rt = $db->where("module='{$module}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$module}不存在,请重试", 'status'=>0));exit;
		}
		$path = APPPATH . "admin/{$rt['module']}.class.php";
		if(!file_exists($path))
		{
			$this->json(array('msg' => "{$module}文件不存在,请重试", 'status'=>0));
		}
		
		//load::loadFile($module,'app/admin/');
		if(!class_exists($module))
		{
			require_once($path);
		}
		

		$rs = $this->help->read($module);
		$rec = $r = array();
		$porder = $db->where("module_id='{$module}'")->getMaxOrderOp()+1;

		foreach($rs as $k => $v)
		{
			$r['module_id'] = $v['class'];
			$r['name'] = $v['method'];
			$r['desc'] = $v['tostring'];
			$r['desc'] = str_replace('/**','',$r['desc']);
			$r['desc'] = str_replace('*/','',$r['desc']);
			$r['desc'] = trim($r['desc']);
			if(substr($r['name'],-6) !== 'action')
			{
				//unset($rs[$k]);
				continue;
			}	
			$r['method'] = substr($r['name'],0,-6);
			$r['name'] = $r['method'];
			$r['is_admin'] = 1;
			$r['status'] = 1;
			$r['is_menu'] = 0;

			$r['porder'] = $porder++;

			$rec[] = $r;
		}
		$cnt = 0;
		foreach($rec as $k => $v)
		{
			//验证是否已经存在,如果不存在,插入
			if($db->checkOpExists($v))
			{
				//p("跳过...".$v['method']);
				continue;
			}
			$db->table('module_op')
			->values($v)
			->add();
			$cnt++;
		}
		$this->json(array('msg' => "成功导入模块[{$rt['name']}] {$cnt}个操作", 'status'=>1));
		
	}
	
	/**
	模块分类,一个简单的一级分类,暂时放弃了
	*/
	public function categoryAxxction()
	{
		
		$this->display();
	}


}