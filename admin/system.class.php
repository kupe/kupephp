<?php
/**
@自动生成 BY Kupe框架 www.kupebank.com
#date : 2018-03-03 16:58:23
#path : app/admin/system.class.php
*/
class system extends Admin
{
	/** */
	public function indexAction()
	{	
		$db = m('config');
		$ajax = intval($this->security->get('ajax'));
		$this->tpl->assign('ajax',$ajax);


		if($ajax)
		{
			$keyword = $this->security->get('keyword');
			$wstr = '';
			if($keyword)
			{
				$wstr = "a.type='{$keyword}'";
			}
			$rs = $db->order("type,porder,id")
				->as('a')	
				->where($wstr)
				->field("a.*")
				->findAll();
			$num = 1;
			foreach($rs as $k => $v)
			{
				$rs[$k]['num'] = $num++;
			}
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',count($rs));
		}
		$list = $db->getTypeList();
		$this->tpl->assign('list',$list);

		$this->display();
	}
	
	/**
	添加模型
	*/
	public function addAction()
	{
		$type = $this->security->get('type'); 
		if($type == 'undefined')
		{
			$type = '';
		}
		$this->tpl->assign('type',$type);
		$db = m('config');
		$this->tpl->assign('porder', $db->getMaxOrder()+1);

		$this->display();
	}
	/**
	处理添加模型
	*/
	public function doaddAction()
	{
		$input = array(); 
		$input['desc'] = $this->security->post('desc'); 
		$input['key'] = $this->security->post('key'); 
		$input['value'] = $this->security->post('value'); 
		$input['type'] = $this->security->post('type'); 
		$input['porder'] = $this->security->post('porder'); 

		!$input['type'] && $input['type'] = '默认';

		$input['porder'] = intval($input['porder']);
		if(!$input['key'])
		{
			$this->json(array('msg' => "标记不能为空", 'status'=>0));
		}
		$db = m('config');
		
		if($db->check($input['key']))
		{
			$this->json(array('msg' => "标记:{$input['key']}已经存在,请换一个", 'status'=>0));
		}
		$input['id'] = $db->values($input)->add();

		#自动添加常用的字段,建表$db->addMode($input);

		$this->json(array('msg' => "添加配置 {$input['key']}成功", 'status'=>1));		
	}
	/***/
	public function editAction()
	{
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('config');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		$rt['desc'] = html_entity_decode($rt['desc']);

		$this->tpl->assign('rt', $rt);
		$this->display();
	}
	/***/
	public function doeditAction()
	{
		$input = array(); 
		
		$input['desc'] = $this->security->post('desc'); 
		$input['key'] = $this->security->post('key'); 
		$input['value'] = $this->security->post('value'); 
		$input['type'] = $this->security->post('type'); 
		$input['porder'] = $this->security->post('porder'); 


		$input['porder'] = intval($input['porder']);	
		$input['id'] = $this->security->post('id'); 
		$input['id'] = intval($input['id']);

		!$input['type'] && $input['type'] = '默认';

		$db = m('config');
		$rt = $db->pk($input['id'])->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}
		#标记不同,验证一下
		if($rt['key'] != $input['key'])
		{
			if($db->where("id!='{$input['id']}'")->check($input['key']))
			{
				$this->json(array('msg' => "标记:{$input['key']}已经存在,请换一个", 'status'=>0));
			}
		}
		$db->values($input)->update();
		$this->json(array('msg' => "编辑配置:{$rt['key']}成功", 'status'=>1));		

	}
	public function dodeleteAction()
	{
	
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('config');

		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "配置记录不存在,请换一个", 'status'=>0));
		}
		$db->pk($id)->delete();
		//$db->where("type=''")->delete();
		$this->json(array('msg' => "删除配置:{$rt['key']} 成功", 'status'=>1));		
	}
	/**
	快速保存
	*/
	function dofastAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->post('id');
		$input['id'] = intval($input['id']);
		$db = m('config');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		if($field == 'key')
		{
			#key不同,验证一下
			$input['key'] = $value;
			if($rt['key'] != $input['key'])
			{
				if($db->where("id!='{$input['id']}'")->check($input['key']))
				{
					$this->json(array('msg' => "标记:{$input['key']}已经存在,请换一个", 'status'=>0));
				}
			}
			//$this->json(array('msg' => "{$field}字段不允许修改,请重试", 'status'=>0));
		}
		$input[$field] = $value;

		$db->values($input)->where("id='{$input['id']}'")->update();
		
		$this->json(array('msg' => "快速编辑配置{$field}=>{$value} 成功", 'status'=>1));
	}
	/**
	
	

	修改分类
	*/
	function edittypeAction()
	{
		$db = m('config');
		$rs = $db->getTypeList();
		$type = "";
		foreach($rs as $k => $v)
		{
			$type .= "{$v} = {$v}\n";
		}
		$this->tpl->assign('type',$type);
		$this->display();
	}

#$db->set('type="全局配置"')->where('type="performace"')->update();
		//$db->set('type="用户自定义"')->where('type="user"')->update();
		//$db->set('type="系统配置"')->where('type="sys"')->update();
	function doedittypeAction()
	{
		$type = $this->security->post('type');
		$db = m('config');
		$list = $db->getTypeList();

		$rs = parse_ini_string($type);
		$cnt = 0;
		foreach($rs as $k => $v)
		{			
			if(!isset($list[$k]) || $v == $list[$k])
			{
				continue;
			}			
			$db->set("type='{$v}'")->where("type='{$k}'")->update();	
			$cnt++;
			//echo "更新" . $db->lastsql();
		}
		$this->json(array('msg' => "保存分类成功,共执行SQL{$cnt}次", 'status'=>1));		
	}
	/**
	功能说明:导航管理
	创建日期:2018-03-10
	*/
	public function indexnavAction()
	{
		$ajax = $this->security->get('ajax');
		$ajax = intval($ajax);
		$this->tpl->assign('ajax',$ajax);
		
		if($ajax)
		{
			//当前页数
			$page = intval($this->security->get('page'));
			//每页显示 $limit 条
			$limit = intval($this->security->get('limit'));
			$keyword = $this->security->get('keyword');
			//权限的意思: 自己不能编辑自己, 可以管理等级比自己低的用户
			$wstr = "";
			if($keyword)
			{
				$wstr = "	a.tag like '%{$keyword}%' OR 
				a.name like '%{$keyword}%' 
				";
			}
			$db = m("nav");
			$total = $db->as('a')->where($wstr)->count();

			$r = $this->getLimit($page,$limit, $total);

			$rs = $db->order("porder,id")
				->limit($r['limit'])
				->as('a')
				->where($wstr)
				->leftJoin("category as b","a.category_id=b.id")
				->field("a.*,b.name as category_name,b.name2")
				->findAll();
			$rs = $db->dealNavUrl($rs);
			
			
			$this->tpl->assign('page',$r['limit']);
			$this->tpl->assign('keyword',$keyword);
			$this->tpl->assign('rs',$rs);
			$this->tpl->assign('total',$r['total']);
		}		
		$this->display();

	}
	/**
	功能说明:	
	创建日期:2018-03-11
	*/
	public function addnavAction()
	{
		
		$ajax = $this->security->get('ajax');
		$ajax = intval($ajax);
		$this->tpl->assign('ajax',$ajax);
		if($ajax==1)
		{
			$category_id = $this->security->get('category_id');
			$url = $this->security->get('url');
			$news_id = $this->security->get('news_id');

			$type = $this->security->get('type');
			if($type == 'my')
			{
				$title = "自定义网址";
				$tpl = "<label class='layui-form-label'>{$title}</label>
    <div class='layui-input-block'>
      <input type='text' name='url' value='{$url}' placeholder='请输入{$title}' autocomplete='off' class='layui-input'>
 <input type='hidden' name='type2' value='my'>
    </div>";	echo $tpl;
				return true;
			}
			$tmp = explode('|',$type);
			if(!isset($tmp[1]))
			{
				echo "参数错误";return true;
			}			
			if($tmp[1] == 'category')
			{
				$tpl = "";
				$title = "分类";
				$tpl = str_replace('{title}', $title, $tpl);
				$module = $tmp[0];
				$mid = $tmp[2];
				
				$db2 = m('category');

				$tree = $db2->where("mid='{$mid}'")->getTreeObj();
				
				$html = $tree->select(array(
				'name' => 'category_id',
				'default' => -1,
				'value' => '',
				'class' => '',
				));

				$tpl = "<label class='layui-form-label'>分类选择</label>
    <div class='layui-input-block'>{$html}
	 <input type='hidden' name='mid' value='{$mid}'>
	  <input type='hidden' name='type2' value='{$tmp[1]}'>
	  </div>";

			}
			elseif($tmp[1] == 'content')
			{
				#显示文章列表
				$db = m('mode');
				$tag = $tmp[0];
				$mid = $tmp[2];
				$table = "my_{$tag}";
				$rs = $db->table($table)->order('id DESC')->field("id,title")->limit(200)->findAll();
				$rec = array();
				$cnt = 1;
				foreach($rs as $k => $v)
				{
					$rec[$v['id']] = $cnt . '.' . $v['title'];
					$cnt++;
				}
				$html = $this->tpl->html_select(
				['options' => $rec, 'name' => 'news_id','default' => '请选择...','value'=>$news_id, 'other'=>'lay-filter="type2" lay-verify="required"']
				,1);


				$title = "{$tag}的ID";
				$tpl = "<label class='layui-form-label'>{$title}</label>
    <div class='layui-input-block'>
				{$html}
	  <input type='hidden' name='mid' value='{$mid}'>
	  <input type='hidden' name='type2' value='{$tmp[1]}'>
    </div>";
				$tpl = str_replace('{title}', $title, $tpl);
				$tpl = str_replace('{mid}', $mid, $tpl);

			}
			echo $tpl;
			
			exit;

		}
		//id = module_id		
		$db = m('nav');
		$this->tpl->assign('porder', $db->max("porder")+1);
		
		$type = $db->getType();

		$this->tpl->assign('type',$type);
		

		$this->display();
	}
	function doaddnavAction()
	{
		
		$input = array(); 
		$input['name'] = $this->security->post('name');
		//$input['type'] = $this->security->post('type'); 
		$input['category_id'] = intval($this->security->post('category_id')); 
		$input['mid'] = intval($this->security->post('mid')); 		
		$input['news_id'] = intval($this->security->post('news_id')); 

		#category , content , my 
		$input['type'] = $this->security->post('type2');
		$input['porder'] = $this->security->post('porder'); 

		
		#自定义网址
		$input['url'] = $this->security->post('url'); 
		$db = m('nav');

		$db->values($input)->add();

		$this->json(array('msg' => "添加导航功能", 'status'=>1));		
	}

	function dofastnavAction()
	{
		$input = array();
		$value = $this->security->http['value'];
		$field = $this->security->http['field'];
		$input['id'] = $this->security->post('id');
		$input['id'] = intval($input['id']);
		$db = m('nav');
		
		$rt = $db->where("id='{$input['id']}'")->find();
		if(!$rt)
		{
			$this->json(array('msg' => "{$input['id']}不存在,请重试", 'status'=>0));
		}
		unset($rt['id']);
		
		if(!isset($rt[$field]))
		{
			$this->json(array('msg' => "{$field}字段{$value}不存在,请重试", 'status'=>0));
		}
	
		if($field == 'status')
		{
			$value = intval($value);
			$value = $value == 0 ? 0 : 1;
		}
		$input[$field] = $value;

		$db->values($input)->where("id='{$input['id']}'")->update();
		
		$this->json(array('msg' => "快速编辑导航{$field}=>{$value} 成功", 'status'=>1));		

	}
	
	/**
	功能说明:	
	创建日期:2018-03-11
	*/
	public function editnavAction()
	{
		/**
		
		$ajax = intval($this->security->get('ajax'));
		$this->tpl->assign('ajax',$ajax);
		if($ajax==1)
		{
			$type = $this->security->get('type');
			if($type == 'my')
			{
				$title = "自定义网址";
				$tpl = "<label class='layui-form-label'>{$title}</label>
    <div class='layui-input-block'>
      <input type='text' name='url' value='' placeholder='请输入{$title}' autocomplete='off' class='layui-input'>
 <input type='hidden' name='type2' value='my'>
    </div>";	echo $tpl;
				return true;
			}
			$tmp = explode('|',$type);
			if(!isset($tmp[1]))
			{
				echo "参数错误";return true;
			}			
			if($tmp[1] == 'category')
			{
				$tpl = "";
				$title = "分类";
				$tpl = str_replace('{title}', $title, $tpl);
				$module = $tmp[0];
				$mid = $tmp[2];
				
				$db2 = m('category');

				$tree = $db2->where("mid='{$mid}'")->getTreeObj();
				
				$html = $tree->select(array(
				'name' => 'category_id',
				'default' => -1,
				'value' => '',
				'class' => '',
				));

				$tpl = "<label class='layui-form-label'>分类选择</label>
    <div class='layui-input-block'>{$html}
	 <input type='hidden' name='mid' value='{$mid}'>
	  <input type='hidden' name='type2' value='{$tmp[1]}'>
	  </div>";

			}
			elseif($tmp[1] == 'content')
			{
				$db = m('mode');
				$tag = $tmp[0];
				$mid = $tmp[2];
				$table = "my_{$tag}";
				$rs = $db->table($table)->order('id DESC')->field("id,title")->findAll();
				$rec = array();
				foreach($rs as $k => $v)
				{
					$rec[$v['id']] = $v['title'];
				}
				$html = $this->tpl->html_select(
				['options' => $rec, 'name' => 'news_id','default' => '请选择...','value'=>'', 'other'=>'lay-filter="type2" lay-verify="required"']
				,1);


				$title = "{$tag}的ID";
				$tpl = "<label class='layui-form-label'>{$title}</label>
    <div class='layui-input-block'>
				{$html}
	  <input type='hidden' name='mid' value='{$mid}'>
	  <input type='hidden' name='type2' value='{$tmp[1]}'>
    </div>";
				$tpl = str_replace('{title}', $title, $tpl);
				$tpl = str_replace('{mid}', $mid, $tpl);

			}
			echo $tpl;
			exit;

		}
		
		*/
		//id = module_id		
		$id = intval($this->security->get('id'));
		$db = m('nav');
		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "记录不存在,请换一个", 'status'=>0));
		}

		$rt['type2'] = $db->getTypeKey($rt, m('mode')->getMode());


		$this->tpl->assign('rt', $rt);

		
		$type = $db->getType();
		$this->tpl->assign('type',$type);
		

		$this->display();

	}	
	/**
	执行编辑执行
	**/
	function doeditnavAction()
	{
		
		$input = array(); 
		
		$input['id'] = intval($this->security->post('id')); 

		$input['name'] = $this->security->post('name');
		//$input['type'] = $this->security->post('type'); 
		$input['category_id'] = intval($this->security->post('category_id')); 
		$input['mid'] = intval($this->security->post('mid')); 		
		$input['news_id'] = intval($this->security->post('news_id')); 

		#category , content , my 
		$input['type'] = $this->security->post('type2');
		$input['porder'] = $this->security->post('porder');
		
		#自定义网址
		$input['url'] = $this->security->post('url'); 

		$db = m('nav');
		$rt = $db->pk($input['id'])->find();
		if(!$rt)
		{
			$this->json(array('msg' => "导航记录不存在,请换一个", 'status'=>0));
		}
		$db->values($input)->update();

		$this->json(array('msg' => "编辑导航:{$rt['name']}成功", 'status'=>1));		
	}
	function dodeletenavAction()
	{
		$id = $this->security->get('id');
		$id = intval($id);
		$db = m('nav');

		$rt = $db->pk($id)->find();
		if(!$rt)
		{
			$this->json(array('msg' => "导航记录不存在,请换一个", 'status'=>0));
		}
		$db->pk($id)->delete();
		//$db->where("type=''")->delete();
		$this->json(array('msg' => "删除导航:{$rt['name']} 成功", 'status'=>1));		

	}

    
}