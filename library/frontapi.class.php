<?php
/**
此类用于前台数据调用


获取参数
{:$kupe->get('login')}

模板自定义变量
{:$kupe->lang('login')}
*/
class frontapi 
{
	
	public $cacheing;
	public $global;
	function __construct()
	{
		$this->cacheing = load::loadClass('cacheing');
		if($this->cacheing->setup()->exists('global'))
		{
			$this->global = $this->cacheing->get('global');
		}
		else
		{
			//生成全局缓存		
			$this->global = $this->makeCache();
		}
	}
	/**
	功能: 生成全局缓存
	*/
	public function makeCache()
	{
		$rec = array();
		//nav
		$rec['nav'] = $this->cacheNav();
		//类型
		$rec['mode'] = $this->cacheMode("id");
		$rec['modetag'] = $this->cacheMode("tag");

		//
		//分类
		$rec['category'] = $this->cacheCategory();

		//后台自定义配置
		$rec['config'] = $this->cacheConfig();

		//ROUTER 前台网址
		$rec['router'] = $this->cacheRouter();

		$this->cacheing->add('global', $rec);
		return $rec;

	}
	function cacheMode($key)
	{
		$db2 = m('mode');
		$modes = $db2->getMode($key);
		return $modes;
	}
	function cacheCategory()
	{
		$db = m('category');
		$rs = $db->getData();
		return $rs;
	}
	function cacheConfig()
	{
		$db = m('config');
		$rs = $db->order("type,porder,id")
				->as('a')		
				->field("*")
				->findAll();
		$rec = array();
		foreach($rs as $k => $v)
		{
			$rec[$v['key']] = $v['value'];
		}
		return $rec;
	}

	function cacheNav()
	{
		$db = m('nav');
		$rs = $db->order("porder,id")
				->as('a')
			
				->leftJoin("category as b","a.category_id=b.id")
				->field("a.*,b.name as category_name,b.name2")
				->findAll();
		
		$rs = $db->dealNavUrl($rs);
		return $rs;
	}

	/**
	功能: 生成 前台路由网址对应表,对应的参数

	*/
	function cacheRouter()
	{
		$rec = array();
		//分类
		$db = m('category');
		$rs = $db->where("`show`=1")->findAll();
		foreach($rs as $k => $v)
		{
			if($v['name2'])
			{
				$rec[$v['name2']] = "c=index&m=category&id={$v['id']}";
			}
			else
			{
				$rec[$v['id']] = "c=index&m=category&id={$v['id']}";
			}			
		}
		//具体内容页
		$db = m('mode');
		$modes = $db->getMode();
		foreach($modes as $k => $v)
		{
			$table = "my_{$v['tag']}";
			$rs = $db->table($table)
				->field('name2,id,title')
				->where("name2!=''")->findAll();
			foreach($rs as $kk => $vv)
			{
				$rec[$vv['name2']] = "c=index&m=content&mode={$v['tag']}&id={$vv['id']}";
			}
		}
		return $rec;
	}

	/**
	功能: 获取导航数据
	*/
	public function nav()
	{
		return $this->global['nav'];
	}
	/**
	功能: 获取分类数据
	*/
	public function category()
	{
		return $this->global['category'];
	}

	/**
	功能: 获取配置数据
	*/
	public function config($key)
	{		
		return isset($this->global['config'][$key]) ? $this->global['config'][$key] : false;
	}
	public function g($key)
	{		
		return $this->global['config'][$key];
	}
	/**
	功能: 获取具体数据
	*/
	public function content($id)
	{

	}
	function id($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	功能: 获取具体数据
	*/
	public function get($key)
	{
		static $s;
		if(!$s)
		{
			$s = load::loadClass('security');
		}		
		return $s->get($key);
	}
	public function lang($key)
	{
		static $lang;
		if(!$lang)
		{
			if(is_file(TEMPLATE_PATH."/cn.lang.php"))
			{
				$lang = parse_ini_file(TEMPLATE_PATH."/cn.lang.php");
			}
			else
			{
				$lang = array();
				return false;
			}					
		}
		return isset($lang[$key]) ? $lang[$key] : false;		
	}
	
}