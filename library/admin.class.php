<?php
#global function 
function showIcon($tpl)
{
	$icon = array(
		'add' => '&#xe61f;',	
		'edit' => '&#xe642;',	
		'del' => '&#xe640;',	
		'主页' => '&#xe68e;',	
		'好友请求' => '&#xe612;',	
		'我的好友' => '&#xe613;',	
		'设置' => '&#xe614;',
		'树' => '&#xe62e;',
		'表格' => '&#xe62d;',
		'搜索' => '&#xe615;',
		);

	if(substr($tpl,0,1) == '&')
	{
		return "<i class=\"layui-icon\" >{$tpl}</i> ";
	}
	elseif(isset($icon[$tpl]))
	{
		return "<i class=\"layui-icon\" >{$icon[$tpl]}</i> ";
	}
	else
	{
		return "<i class=\"layui-icon\" >{$icon['设置']}</i> ";;
	}
}

/**主要用于模板的权限验证 BY 边缘狂人 2018年3月3日 */
function checkAuth($c, $m)
{
	if (
		(is_string($_SESSION['user']['auth'])  && $_SESSION['user']['auth']== '*')
		|| in_array($c.'_'.$m,$_SESSION['user']['auth']))
	{
		return true;
	}
	return false;
}
/**

*/

class admin extends control
{
	function __construct()
	{
		
		session_start();
		if(
			($this->router->c == 'index' && $this->router->m == 'login') OR 
			($this->router->c == 'index' && $this->router->m == 'dologin')
			
		)
		{			
			
		}	
		else
		{
			$this->checkAuth();
		}
		$skin = __ROOT__ . '/template/' . getconfig('template_dir') . '/';
		$this->tpl->assign('SKIN',$skin);

		$skin2 = APPPATH . '/template/' . getconfig('template_dir') . '/';
		$this->tpl->assign('__SKIN__',$skin2);
		


		$this->tpl->assign('STATIC', __ROOT__ . '/static/');
		
		if(substr($this->router->m,0,2) == 'do')
		{
			#刷新缓存
			$this->cacheing->delete('global');
		}
		
	}
	function display($file = '', $sep = '.')
	{
		$icon = array(
		'add' => '&#xe61f;',	
		'edit' => '&#xe642;',	
		'del' => '&#xe640;',	
		'主页' => '&#xe68e;',	
		'好友请求' => '&#xe612;',	
		'我的好友' => '&#xe613;',	
		);
		foreach($icon as $k => $v)
		{
			$icon[$k] = '<i class="layui-icon">'.$v . '</i>';
		}
		$this->tpl->assign('ICON',$icon);

		parent::display($file, $sep);
	}


	function checkAuth()
	{

		if(!isset($_SESSION['user']['level']))
		{
			//echo "没有登陆,转向";
			$url = $this->router->url('index','login');
			$this->router->jump($url);

		}
		elseif($_SESSION['user']['level'] > 2)
		{

		}
		else
		{			
			//验证权限
			if(!in_array($this->router->c . '_' . $this->router->m, $_SESSION['user']['auth']))
			{
				$msg = "您没有权限访问这个模块 => " . $this->router->c . '_' . $this->router->m;
				echo "{$msg}";exit;
				$this->json( 
				array('msg' => $msg,'status'=>0)	
				);
				
			}
		}
	}
	function json($msg)
	{
		if(isset($msg['msg']))
		{
			$path = APPPATH . "/data/log.txt";
			$content = date('Y-m-d H:i:s')."\t{$msg['msg']}\n";
			$fp = fopen($path, 'a');
			fwrite($fp,$content);
		}
		
		echo json_encode($msg);
		exit;
	}

	/** 
		Home
	*/
	# $total 总数 , $page 当前页 ,  $limit 每页显示多少条
	function getLimit($page, $limit, $total)
	{
		
		$limit <= 0 && $limit = 10;		
		$page <= 0 && 	$page = 1;

		$total_page = ceil($total / $limit);
		$page > $total_page && 	$page = $total_page;		

		
		$r['limit'] = ($page-1)*$limit;
		if($r['limit'] < 0)
		{
			$r['limit'] = 0;
		}
		$r['limit'] = "{$r['limit']},{$limit}";
		$r['total'] = $total;
		$r['page'] = $page;
		return $r;
	}
}