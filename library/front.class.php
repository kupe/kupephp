<?php

/**主要用于模板的权限验证 BY 边缘狂人 2018年3月3日 */
function checkAuth($c, $m)
{
	if (
		(is_string($_SESSION['user']['auth'])  && $_SESSION['user']['auth']== '*')
		|| in_array($c.'_'.$m,$_SESSION['user']['auth']))
	{
		return true;
	}
	return false;
}
/**

*/

class front extends control
{
	public $frontapi;
	function __construct()
	{
		//$skin = getconfig('application_dir') . '/template/' . getconfig('template_dir') . '/';
		
		$this->tpl->assign('SKIN', SKIN . "/");
		$this->tpl->assign('STATIC', __ROOT__ . '/static/');

		
		$this->frontapi = load::loadClass('frontapi');

		
		$this->tpl->assign('kupe',$this->frontapi);
		//$this->tpl->registerObject('kupe',load::loadClass('frontapi'));

	}
	function display($file = '')
	{
		$icon = array(
		'add' => '&#xe61f;',	
		'edit' => '&#xe642;',	
		'del' => '&#xe640;',	
		'主页' => '&#xe68e;',	
		'好友请求' => '&#xe612;',	
		'我的好友' => '&#xe613;',	
		);
		foreach($icon as $k => $v)
		{
			$icon[$k] = '<i class="layui-icon">'.$v . '</i>';
		}
		$this->tpl->assign('ICON',$icon);

		
		parent::display($file);
	}


	function json($msg)
	{
		if(isset($msg['msg']))
		{
			$path = APPPATH . "data/log.txt";
			$content = date('Y-m-d H:i:s')."\t{$msg['msg']}\n";
			$fp = fopen($path, 'a');
			fwrite($fp,$content);
		}
		
		echo json_encode($msg);
		exit;
	}

	/** 
		Home
	*/
	# $total 总数 , $page 当前页 ,  $limit 每页显示多少条
	function getLimit($page, $limit, $total)
	{
		
		$limit <= 0 && $limit = 10;		
		$page <= 0 && 	$page = 1;

		$total_page = ceil($total / $limit);
		$page > $total_page && 	$page = $total_page;		

		
		$r['limit'] = ($page-1)*$limit;
		if($r['limit'] < 0)
		{
			$r['limit'] = 0;
		}
		$r['limit'] = "{$r['limit']},{$limit}";
		$r['total'] = $total;
		$r['page'] = $page;
		return $r;
	}
	 function page_url($url){
		$u = $this->router->url($this->router->c, $this->router->m,$url);
        return $u;
    }
	function pageCompile($data)
	{
		$data = str_replace('router.php','index.php', $data);


		return $data;

	}

	function page($total, $subUrl = '', $pageSize = 20)
	{
        if ($pageSize < 1){
            $pageSize = 20;
        }
        $maxNum = 10;
		


        $page = $this->security->get('page');


        $maxPage = ceil($total / $pageSize);
        if ($page < 0){
            $page = $prev = 0;
        }elseif ($page >= $maxPage){
            $page = $maxPage-1;
        }
        $base_url = $this -> page_url("{$subUrl}");
        $__out = "<script>function page_go(page_num){
		url = '{$base_url}' + '&page=' + page_num;		
		 self.location.href=url;
		}
		</script>
		";
        $__out .= '<ul class="am-pagination am-pagination-centered am-pagination-right">';
        $__out .= "<li class=''><span href='#'>#stat#</span></li>";
        if($page-1 < 0){
            $__out .= "<li  class=\"am-disabled\"><span>«</span></li>";
            $__out .= "<li  class=\"am-disabled\"><span>prev</span></li>";
        }else{
            $__out .= "<li><a href='" . $this -> page_url("{$subUrl}") . "'>«</a></li>";

			if($page-1 <= 0)
			{
				$__out .= "<li><a href='" . $this -> page_url($subUrl) . "'>prev</a></li>";
			}
			else
			{
	            $__out .= "<li><a href='" . $this -> page_url($subUrl . '&page=' . ($page-1)) . "'>prev</a></li>";
			}
        }
        $half = ceil($maxNum / 2) ;
        $begin = ceil($page - $half);
        if ($begin < 0){
            $begin = 0;
        }
        for($i = $begin, $k = 0; $i < $maxPage && $k < $maxNum; ++$i){
            $j = $i + 1;
            $k++;
            if ($i == $page){
                $__out .= "<li class='active'><span >{$j}</span></li>";
            }else{
				if($i)
				{
					$__out .= "<li><a href='" . $this -> page_url($subUrl . '&page=' . $i) . "'>{$j}</a></li>";
				}
				else
				{
					$__out .= "<li><a href='" . $this -> page_url($subUrl) . "'>{$j}</a></li>";
				}
            }
        }
        if($maxPage <= ($page + 1)){
            $__out .= "<li  class=\"am-disabled\"><span>next</span></li>";
        }else{
            $__out .= "<li><a href='" . $this -> page_url($subUrl . '&page=' . ($page + 1)) . "'>next</a></li>";
        }
        if($maxPage == 1 || $maxPage == $page + 1){
            $__out .= "<li  class=\"am-disabled\"><span>»</span></li>";
        }else{
            $__out .= "<li><a href='" . $this -> page_url($subUrl . '&page=' . ($maxPage-1)) . "'>»</a></li>";
        }
        $__out .= '</ul><div style="clear:both;"></div>';
        $offset = $page * $pageSize;
        $offset < 0 && $offset = 0;
        $data['limit'] = "{$offset},{$pageSize}";
        $data['page'] = $__out;
        $data['stat'] = "<span class='stat' style='margin-top:10px;'>";
        $data['stat'] .= sprintf("共有:%d/%d页,共:%d条,每页:%d条", ($page + 1), $maxPage, $total, $pageSize);
        $data['stat'] .= '</span>';
        $data['page'] = str_replace('#stat#', $data['stat'], $data['page']);
		$data['total'] = $total;

		
		####编译
		$data['page'] = $this->pageCompile($data['page']);
		####编译

        return $data;
    }
}