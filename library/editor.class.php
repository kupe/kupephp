<?php

class editor
{
	#编辑器始始化
	function kingedit4($var)
	{
		static $toolbarset;
		static $count = 0;

		extract($var);

	//	extract($var, array('id', 'toolbarset', 'height', 'width', 'name', 'value' ));
		$width = isset($width) ? $width : "700px";
		$height = isset($height) ? $height : "400px";
		$value = isset($value)? stripslashes($value) : "";
		
		$basePath	= getconfig('application_dir') . '/editor/kindeditor4/';

		$items = array(
		'default'	=> "",
		'simple' => "items : [
				'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'image', 'link'],",
		'txt' => "items : [
				'source','fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'link'],",
		
		);
		
		$toolbarset = isset($items[$toolbarset]) ? $toolbarset : 'default';
		



		$__ctl_out = "";
		if($count == 0)
		{
			$__ctl_out .= "<script charset='utf-8' src='{$basePath}kindeditor-all-min.js'></script>";
			$__ctl_out .= "<script charset='utf-8' src='{$basePath}lang/zh-CN.js'></script>";
		}
		++$count;


		
		$__ctl_out .= "
		<script>
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name=\"{$name}\"]', {
					allowFileManager : true,
					resizeType : 1,
					langType : 'zh-CN',
					//指定编辑器iframe document的CSS，用于设置可视化区域的样式。
					//cssPath : ['css/bootstrap.min.css','template/default/css/addon.css'],
					allowPreviewEmoticons : false,
					{$items[$toolbarset]}
					allowImageUpload : true,
					filterMode:false
				});
			});
		</script>
			

		</script>";

		$__ctl_out .= "<textarea id='{$name}' name='{$name}' cols='100' rows='8' style='width:{$width};height:{$height};visibility:hidden;'>{$value}</textarea>";

		/*$__ctl_out .= $this->uploadpic(array(
			'name' => 'edit_pic',
			'value' => '',			
			'hidden' => 1,	
			'callback' => "edit_pic_insert",	
			));*/

		return $__ctl_out;
	}
	function kingedit4__($var)
	{
		static $toolbarset;
		static $count = 0;

		extract($var);

	//	extract($var, array('id', 'toolbarset', 'height', 'width', 'name', 'value' ));
		$width = isset($width) ? $width : "700px";
		$height = isset($height) ? $height : "400px";
		$value = isset($value)? stripslashes($value) : "";
		
		$basePath	= getconfig('application_dir') . '/editor/kindeditor4/';

		$items = array(
		'default'	=> "",
		'simple' => "items : [
				'fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'emoticons', 'image', 'link'],",
		'txt' => "items : [
				'source','fontname', 'fontsize', '|', 'textcolor', 'bgcolor', 'bold', 'italic', 'underline',
				'removeformat', '|', 'justifyleft', 'justifycenter', 'justifyright', 'insertorderedlist',
				'insertunorderedlist', '|', 'link'],",
		
		);
		$toolbarset = isset($items[$toolbarset]) ? $toolbarset : 'default';
		



		$__ctl_out = "";
		if($count == 0)
		{
			$__ctl_out .= "<script charset='utf-8' src='{$basePath}kindeditor-min.js'></script>";
			$__ctl_out .= "<script charset='utf-8' src='{$basePath}lang/cn.js'></script>";
		}
		++$count;


		
		$__ctl_out .= "
		<script>
			var editor;
			KindEditor.ready(function(K) {
				editor = K.create('textarea[name=\"{$name}\"]', {
					allowFileManager : true,
					resizeType : 1,
					langType : 'cn',
					//指定编辑器iframe document的CSS，用于设置可视化区域的样式。
					//cssPath : ['css/bootstrap.min.css','template/default/css/addon.css'],
					allowPreviewEmoticons : false,
					{$items[$toolbarset]}
					allowImageUpload : true,
					filterMode:false
					
				});				
			});
			function edit_pic_insert(v){
			editor.insertHtml(\"<img src='\"+v+\"'>\");
	}

		</script>";

		$__ctl_out .= "<textarea id='{$name}' name='{$name}' cols='100' rows='8' style='width:{$width};height:{$height};visibility:hidden;'>{$value}</textarea>";

		/*$__ctl_out .= $this->uploadpic(array(
			'name' => 'edit_pic',
			'value' => '',			
			'hidden' => 1,	
			'callback' => "edit_pic_insert",	
			));*/

		return $__ctl_out;
	}
}