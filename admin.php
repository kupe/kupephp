<?php
/*
框架流程

加载核心类	z
加载CONFIG	config
初始化环境	init
加载安全	security
定义常量	const

加载
	control.class.php	控制器
	model.class.php		数据库操作类
	tpl.class.php		模板类
	error.class.php		捕捉错误异常等提醒
URL路由
	control	method
	route.class.php		根据URL,载入类

实例化当前类
*/

//define("APPPATH", dirname(__FILE__) . '');
define("APPPATH", str_replace('\\', '/', __DIR__ . ''));


require("kupe/kupe.php");

require('library/admin.class.php');


$config = array(
'control_dir' => 'admin',
'template_dir' => 'admin',

);

$KUPE = new kupe;

//默认'app'
$KUPE->run($config);



